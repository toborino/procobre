let init = (  ) => {

  $('body').imagesLoaded()
    .always( function( instance ) { 
      _close();
    })
    .done( function( instance ) {
      _close();
    })
    .fail( function() {
      _close();
    });

};

let _close = (  ) => {

  $('#loading').fadeOut( 800, function() {
    $('#loading').remove();
  });

}

export default {
    init
};