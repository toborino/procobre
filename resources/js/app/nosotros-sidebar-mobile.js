let init = () => {

	$('.selector-1').click(function(){
			nosotrosSidearMobile('1');
			$('.sd-body-menu').slideUp();
	});
	$('.selector-2').click(function(){
			nosotrosSidearMobile('2');
			$('.sd-body-menu').slideUp();
	});
	$('.selector-3').click(function(){
			nosotrosSidearMobile('3');
			$('.sd-body-menu').slideUp();
	});
	$('.selector-4').click(function(){
			nosotrosSidearMobile('4');
			$('.sd-body-menu').slideUp();
	});
	$('.selector-5').click(function(){
			nosotrosSidearMobile('5');
			$('.sd-body-menu').slideUp();
	});

    $('.selector-6').click(function(){
        nosotrosSidearMobile('6');
        $('.sd-body-menu').slideUp();
    });

	function nosotrosSidearMobile(num) {
    for (var i = 1; i <= 5; i++) { 
      if (i != num) {
        $(".fa-" + i).removeClass('active');
      }
      $(".fa-" + num).addClass('active');
    }
  }

  $('.sd-body-menu a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
	});

	$(".nosotros-sidebar-mobile").sticky({ 
		topSpacing: 0
	});

};
 
export default { 
    init
};
