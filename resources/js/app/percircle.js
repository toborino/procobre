
let init = () => {
	return $('.circle-copper').each(function(i, e) {
		load( $(e) )
	})
};

let load = (element) => {

	  let number = element.find('.valor');
	  let circle = element.find('.front');
	  let v = number.data('percentage');
	 
	  if (v == undefined || v > 100 || v < 0) {
	    return false;
	  }

	  v = 100 - v;

	  var total = 252;

	  var per = (total * v / 100) + 48;

	  circle.css('strokeDashoffset', per);

}
 
export default { 
    init
};