let init = () => {

    var init, layout;
    init = function() {
      var debounce;
      var vWidth = $(window).width(); 
      if ($('[data-equalize]').length && vWidth > 767) {
        debounce = false;
        $(window).on('resize', function() {
          clearTimeout(debounce);
          debounce = setTimeout(layout, 250);
          return layout();
        });
        $(window).resize();
        return $(window).one('load', function() {
          return layout();
        });
      }
    };
    layout = function() {
      var biggest, biggestHeight, currentHeight, equalizeGroups, group, groupId, item, j, k, key, len, len1, results, toEqualize, usesInner;
      toEqualize = $('[data-equalize]');
      equalizeGroups = {};
      for (j = 0, len = toEqualize.length; j < len; j++) {
        item = toEqualize[j];
        groupId = $(item).data('equalize');
        if (!equalizeGroups[groupId]) {
          equalizeGroups[groupId] = [];
        }
        equalizeGroups[groupId].push(item);
      }
      results = [];
      for (key in equalizeGroups) {
        group = equalizeGroups[key];
        group = $(group);
        if (group.attr('data-equalize-inner')) {
          usesInner = true;
        } else {
          group.height('auto');
          usesInner = false;
        }
        biggest = [];
        biggestHeight = 0;
        for (k = 0, len1 = group.length; k < len1; k++) {
          item = group[k];
          if (usesInner) {
            currentHeight = $(item).children('.equalize-inner').height();
          } else {
            currentHeight = $(item).height();
          }
          if (currentHeight > biggestHeight) {
            biggest = $(item);
            biggestHeight = currentHeight;
          }
        }
        if (usesInner) {
          results.push(group.height($(biggest).children('.equalize-inner').height()));
        } else {
          results.push(group.height($(biggest).height()));
        }
      }
      return results;
    };
    
    return init();

}

export default {
    init
};