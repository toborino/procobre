let init = () => {
	$(".sticker").sticky({ 
		topSpacing: 200,
		bottomSpacing: 496
	});
};

export default {
    init
};