let imagesLoaded = require('imagesloaded');

let init = () => {

    //$('.hero-scroll').click( function() {
    //    $('html, body').animate({
    //        scrollTop: $('#scroll-target').offset().top
    //    }, 1000);
    //});

    imagesLoaded.makeJQueryPlugin( $ );

    $('.slider-home').each( function(i, e) {
        load( $(e) );
    });

}

let load = ( $slider ) => {

    $slider.on( 'init', function(event, slick) {
        $('.count').text(slick.slideCount);

        setTimeout( function() {
            $('.count, .hero-slider-tools').fadeIn(1000);
       }, 1000);   

        animation( $slider.find('.slick-slide[data-slick-index="0"]') );
    });

    $slider.slick({
        autoplay: true,
        autoplaySpeed: 10000,
        arrows: false,
        cssEase: 'ease-out',
        draggable: true,
        fade: true,
        speed: 500,
        swipeToSlide: true, 
        responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true
          }
        }
      ]
    });

    $slider.on( 'beforeChange', function(event, slick, currentSlide, nextSlide) {
        $slider.find('.slider-home-image.bgZoom').removeClass('bgZoom');
        $slider.find('.title-slider.fadeInUp ,paragrap.fadeInUp').removeClass('fadeInUp');
        $slider.find('.paragrap.fadeInUp').removeClass('fadeInUp');
        $slider.find('.button.fadeInUp').removeClass('fadeInUp');

        animation( $slider.find('.slick-slide[data-slick-index="' + nextSlide + '"]') );
    });

}

let animation = ( $element ) => {

    $element.imagesLoaded( {
        background: '.slider-home-image'
    }, function() {
        $element.addClass('loaded');
        $element.find('.slider-home-image').addClass('animated bgZoom');
        $element.find('.title-slider ,paragrap').addClass('animated fadeInUp');
        $element.find('.paragrap').addClass('animated fadeInUp');
        $element.find('.button').addClass('animated fadeInUp');
    });
}

export default {
    init
}