let imagesLoaded = require('imagesloaded');

let init = () => {
  imagesLoaded.makeJQueryPlugin( $ );
};

export default {
    init
};