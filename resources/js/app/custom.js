let init = () => {

  // filtro de ica en el mundo

  $('.tab-buttons-1').click(function(){
  		filtroIca('1');
      $('.ica-cuerpo').slideUp();
  });
  $('.tab-buttons-2').click(function(){
  		filtroIca('2');
      $('.ica-cuerpo').slideUp();
  });
  $('.tab-buttons-3').click(function(){
  		filtroIca('3');
      $('.ica-cuerpo').slideUp();
  });
  $('.tab-buttons-4').click(function(){
  		filtroIca('4');
      $('.ica-cuerpo').slideUp();
  });
  $('.tab-buttons-5').click(function(){
  		filtroIca('5');
      $('.ica-cuerpo').slideUp();
  });
    $('.tab-buttons-6').click(function(){
        filtroIca('6');
        $('.ica-cuerpo').slideUp();
    })
    $('.tab-buttons-7').click(function(){
        filtroIca('7');
        $('.ica-cuerpo').slideUp();
    })
    $('.tab-buttons-8').click(function(){
        filtroIca('8');
        $('.ica-cuerpo').slideUp();
    })
    $('.tab-buttons-9').click(function(){
        filtroIca('9');
        $('.ica-cuerpo').slideUp();
    })
    $('.tab-buttons-10').click(function(){
        filtroIca('10');
        $('.ica-cuerpo').slideUp();
    })

  function filtroIca(num) {
    for (var i = 1; i <= 10; i++) {
      if (i != num) {
        $(".tab-buttons-" + i).removeClass('active');
        $(".ica-body-" + i).delay(.5);
        $(".ica-body-" + i).slideUp();
      }
      $(".tab-buttons-" + num).addClass('active');
      $(".ica-body-" + num).delay(.5);
      $(".ica-body-" + num).slideDown();
    }
  }

  $('.scroll-nav__item-1').click(function(){
      navigationSidebar('1');
  });
  $('.scroll-nav__item-2').click(function(){
      navigationSidebar('2');
  });
  $('.scroll-nav__item-3').click(function(){
      navigationSidebar('3');
  });
  $('.scroll-nav__item-4').click(function(){
      navigationSidebar('4');
  });
  $('.scroll-nav__item-5').click(function(){
      navigationSidebar('5');
  });
  
  function navigationSidebar(num) {
    for (var i = 1; i <= 5; i++) { 
      if (i != num) {
        $(".scroll-nav__item-" + i).removeClass('current-active');
      }
      $(".scroll-nav__item-" + num).addClass('current-active');
    }
  }

  $('#content-button').click(function(){ 
    if ($('#menu-mobile').hasClass('open')){
        $('#menu-mobile').removeClass('open');
        $('#blur').removeClass('is-active-blur');
    }else{
        $('#menu-mobile').addClass('open animate-on');
        $('#blur').addClass('is-active-blur animate-on');
    }
  });

  $('#content-button').click(function(){ 
    if ($(this).hasClass('desplaza-btn')){
        $('#content-button').removeClass('desplaza-btn');
        $(this).find('button').removeClass('is-active');
    }else{
        $('#content-button').addClass('desplaza-btn animate-on');
        $(this).find('button').addClass('is-active');
    }
  });

  $('.button-search').click(function(){
    if ($('#content-search').hasClass('content-search-on')){
      $('#content-search').removeClass('content-search-on');
    }else{
      $('#content-search').addClass('content-search-on content-search-animate');
    }
  });

  $('.open-login').click(function(e){
    e.preventDefault();
    $('.content-login').addClass('visible-on');
  })
  $('.close-signin').click(function(e){
    $('.content-login').removeClass('visible-on');
  })

  $('.button-search-mobile').click(function(){
    $('.search-mobile').addClass('search-mobile-on animate-on');
  });
  /*$('.close-search').click(function(){
    $('.search-mobile').removeClass('search-mobile-on animate-on');
  });*/

  $('.content-button-caws').click(function(){
    $('.login-mobile').addClass('login-on');
  });
  $('.signin .close-signin-mobile').click(function(){
    $('.login-mobile').removeClass('login-on');
  });
};


export default {
    init
};