let init = () => {

    $('.filterCategory').change(function() {


        let valor = $(this).val();

        let category = $('.current-category').attr('data-cat');

        $.ajax({
            url: themosis.ajaxurl,
            type: 'POST',
            //dataType: 'html',
            data: {
                action: 'filter_publications',
                cat: valor,
                current_cat:category
            }
        }).done(function(data){
            $('.new-filter-product').html(data);
        });

    });


    $(document).on("click", ".pagitantionProduct a", function () {

        var accion = $(this).attr('data-accion');
        if (accion == 'anterior') {
            var actual = $('.pagitantionProduct .page_actual').text();
            var total = $('.pagitantionProduct .page_total').text();
            var actual = parseInt(actual) - 1;

            var page = actual;
            $('.pagitantionProduct .page_actual').text(page);
            if (page === 1) {
                $('.pagitantionProduct .prev').css('display', 'none');
            }
            if (actual < total) {
                $('.pagitantionProduct .next').css('display', 'inline-block');
            }

        }else if(accion == 'siguiente'){
            var actual = $('.pagitantionProduct .page_actual').text();
            var total = $('.pagitantionProduct .page_total').text();
            var actual = parseInt(actual) + 1;

            var page = actual;

            $('.pagitantionProduct .page_actual').text(page);
            if (page > 1) {
                $('.pagitantionProduct .prev').css('display', 'inline-block');
            }

            if (actual == total) {
                $('.pagitantionProduct .next').css('display', 'none');
            }
        }else{
            var page = $(this).attr('data-page');
            if (page == 1) {
                $('.pagitantionProduct .prev').css('display', 'none');
            }
            if (page > 1) {
                $('.pagitantionProduct .prev').css('display', 'inline-block');
            }
        }

        var limit = $(this).attr('data-pp');


        $(".new-filter-product").html('<div id="loader"><center><i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i><br /><span class="sr-only">Enviando...</span></center></div>');

        $.ajax({
            url: themosis.ajaxurl,
            type: 'POST',
            dataType: 'html',
            data: 'action=paginator_publication&page='+page+'&limit='+limit,
        }).done(function(data){
            $('.new-filter-product').html(data);

        });
        return false;

    });

};

export default {
    init
};