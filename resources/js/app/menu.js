window.Headroom = require('headroom.js');

let init = () => {

  var $header = $('#headroom-menu');
  
  $(window).bind("scroll" ,function(){

    var _top = parseInt( $('body').scrollTop() );

  });

  // menu js
  var navbar = document.querySelector("#headroom-menu");
  var headroom  = new Headroom(navbar, {
    "offset": 200, 
    onUnpin: function() {
        $('#content-search').removeClass('content-search-on');
     }
  });
  
  headroom.init();

};

export default {
    init
};