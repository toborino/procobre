let init = (  ) => {

  var $trigger = $('[data-youtube]');
  var $popup = $('#youtube');
  var $close = $('button.youtube-close');

  var _iframe = '<iframe width="560" height="315" src="https://www.youtube.com/embed/[ID]?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>';
  var _id = undefined;


  $trigger.bind("click",function(){
    $popup.addClass('is-active');
    _id = $(this).attr("data-youtube");
    _iframe = _iframe.replace("[ID]", _id);
    $popup.find('iframe').remove();
    $popup.find('.overview').append( _iframe );
  });

  $close.bind("click",function(){
    $popup.removeClass('is-active');
    $popup.find('iframe').remove();
  });

};

export default {
    init
};