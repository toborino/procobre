let init = () => {
  $( ".datepicker" ).monthpicker({
    changeYear:true,
    minDate: "-3 M",
    maxDate: "+2 Y", 
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio', 'Julio','Agosto','Septiembre','Octubre', 'Noviembre','Deciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun', 'Jul','Ago','Sept','Oct', 'Nov','Dec']
  });
};

export default {
    init
};