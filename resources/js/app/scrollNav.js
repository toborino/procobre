let init = () => {
	$('.scroll-nav__link').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
	});
};
 
export default { 
    init
};