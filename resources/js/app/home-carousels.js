let init = () => {

	$('.content-target-benefits').slick({
	  arrows: true,
	  adaptiveHeight: true,
	  autoplay: true,
   	  autoplaySpeed: 5000,
  	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	      	arrows: false,
	      	dots: true,
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	      	arrows: false,
	      	dots: true,
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	      	arrows: false,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots: true
	      }
	    }
	  ]
	});
};

export default {
    init
};
