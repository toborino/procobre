<?php
/**
 * Created by PhpStorm.
 * User: cGarcia
 * Date: 07-12-17
 * Time: 14:58
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;


class MembersArticlesController extends BaseController
{
    public function membersArticles($post){

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $pagemenber = get_post($post->ID);

        $title = $pagemenber->post_title;
        $content = $pagemenber->post_content;

        $categories = get_terms( 'Categorias', array(
            'hide_empty' => 0,
        ) );

        $cate_type = get_terms( 'cat_type', array(
            'hide_empty' => 0,
        ) );


        $post_publications = new \WP_Query(
            array(
                'post_type' => 'publicacion',
                'showposts' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'usuario_registrado',
                        'value' => '1',
                        'compare' => 'LIKE'
                    )
                ),/*
                'tax_query' => array(
                    array(
                        'taxonomy' => 'cat_type',
                        'field'    => 'slug',
                        'terms'    => 'noticias',
                    ),
                )*/
            )
        );

        if($post_publications->have_posts()){
            while ($post_publications->have_posts()){
                $post_publications->the_post();

                $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];
                if(!empty($termsType)) {
                    $type_cat = $termsType->name;
                }else{
                    $type_cat = '';
                }
                $image_featured =  get_field('featured_image');
                if(!empty($image_featured)){
                    $image_featured = $image_featured['url'];
                }else{
                    $image_featured = '';
                }

                $short_description = get_field('short_description');

                $publications[] = array(
                    'title'         => get_the_title(),
                    'description'   => $short_description,
                    'image'         => $image_featured,
                    'type_cat'      => $type_cat ?? '',
                    'date'          => get_the_date(),
                    'link'          => get_the_permalink()
                );
            }
        }else{
            $publications = '';
        }

        $count_post =  array(
            'post_type' => 'publicacion',
            'showposts' => -1,
            'meta_query' => array(
                array(
                    'key' => 'usuario_registrado',
                    'value' => '1',
                    'compare' => 'LIKE'
                )
            ),
           /* 'tax_query' => array(
                array(
                    'taxonomy' => 'cat_type',
                    'field'    => 'slug',
                    'terms'    => 'noticias',
                ),
            ),*/
        );

        $count_publication = get_posts($count_post);

        return view('miembro-articulos', compact( 'banner' , 'title','content', 'publications', 'cate_type','link_cat','count_publication'));

    }
}