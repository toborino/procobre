<?php
/**
 * User: cgarcia
 * Date: 23-11-17
 * Time: 14:33
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;


class AplicationsController extends BaseController
{
    public function show($post){

        global $wpdb;

        $single = get_post($post->ID);

        $banner = get_field('banner', $post->ID);
        if(!empty($banner)){
            $banner = $banner['url'];
        }else{
            $banner;
        }

        $current_slug = $single->post_name;
        $title_single = $single->post_title;
        $content_image = get_field('content_image', $single->ID);
        if(!empty($content_image)){
            $content_image = $content_image['url'];
        }else{
            $content_image;
        }
        $content_single = get_field('content', $single->ID);

        $post_aplications = new \WP_Query(
            array(
                'post_type' => 'aplications',
                'showposts' => '10'
            )
        );

        if($post_aplications->have_posts()){
            while ($post_aplications->have_posts()){
                $post_aplications->the_post();

                $aplications[] = array(
                    'title'        => get_the_title(),
                    'slug'         => basename(get_permalink()),
                    'link'         => get_the_permalink(),
                );
            }
        }else{
            $aplications = '';
        }

        return view( 'single-aplication', compact('banner','aplications', 'current_slug', 'title_single','content_single', 'content_image'));
    }
}