<?php
/**
 * User: cGarcia
 * Date: 05-12-17
 * Time: 14:31
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;

class WebsitesController extends BaseController
{

    public function websites($post){

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }
        $subtitle = get_field('subtitle');
        $post_copper = get_post($post->ID);
        $content = $post_copper->post_content;

        $offices = get_field('Offices', 269);
        return view('copper-alliance-website', compact('banner', 'subtitle', 'content', 'offices'));
    }
}