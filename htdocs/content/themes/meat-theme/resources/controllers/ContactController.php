<?php
/**
 * User: cGarcia
 * Date: 05-12-17
 * Time: 14:31
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;

class ContactController extends BaseController
{

    public function contact($post){

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }
        $post_contact = get_post($post->ID);
        $content = $post_contact->post_content;
        return view('contacto', compact('banner', 'content'));
    }
}