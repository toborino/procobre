<?php
/**
 * User: cGarcia
 * Date: 22-11-17
 * Time: 12:46
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;


class BenefitsController extends BaseController
{
    public function benefits($post)
    {
        global $post;
        $post = get_post($post->ID);
        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $wp_query = new \WP_Query();
        $all_wp_pages = $wp_query->query(array('post_type' => 'page', 'posts_per_page' => -1));
        $page_children = get_page_children($post->ID, $all_wp_pages);

        $short_description = get_field('short_description');
        $title = $post->post_title;
        $content = $post->post_content;

        return view('beneficios-sociales', compact('banner', 'short_description', 'page_children' , 'title','content'));
    }

    public function benefitsEsencialidad($post){
        global $post;
        $post = get_post($post->ID);

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $content = $post->post_content;
        $page_children = $this->page_parent($post->ID);

        $pagename = get_query_var('pagename');
        if ( !$pagename && $id > 0 ) {
            $post = $wp_query->get_queried_object();
            $pagename = $post->post_name;
        }
        $title = get_the_title($post->ID);

        return view('beneficios-sociales-escenciabilidad', compact('title','banner','content', 'page_children', 'pagename'));
    }

    public function benefitsReciclabilidad($post){
        global $post;
        $post = get_post($post->ID);

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $content = $post->post_content;
        $page_children = $this->page_parent($post->ID);

        $pagename = get_query_var('pagename');
        if ( !$pagename && $id > 0 ) {
            $post = $wp_query->get_queried_object();
            $pagename = $post->post_name;
        }
        $title = get_the_title($post->ID);

        $features = get_field('features');

        $image_video = get_field('image_sub', $post->ID);
        if(!empty($image_video)){
            $image_video = $image_video['url'];
        }else{
            $image_video = '';
        }

        $video_id = get_field('video_id', $post->ID);
        return view('beneficios-sociales-reciclabilidad', compact('title','banner','content', 'page_children', 'pagename', 'features', 'image_video','video_id'));
    }

    public function benefitsSustentabilidad($post){

        global $post;
        $post = get_post($post->ID);


        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $content = $post->post_content;
        $page_children = $this->page_parent($post->ID);

        $pagename = get_query_var('pagename');
        if ( !$pagename && $id > 0 ) {
            $post = $wp_query->get_queried_object();
            $pagename = $post->post_name;
        }
        $title = get_the_title($post->ID);

        $features = get_field('features');

        $image_video = get_field('image_sub', $post->ID);
        if(!empty($image_video)){
            $image_video = $image_video['url'];
        }else{
            $image_video = '';
        }

        $video_id = get_field('video_id', $post->ID);

        $gallery = get_field('gallery', $post->ID);
        if(!empty($gallery)){
            foreach ($gallery as $gallery_value){
                $gallery_page[] = array(
                    'image'         => $gallery_value['image']['url'],
                    'title'         => $gallery_value['title'],
                    'video_id'      => $gallery_value['id_video'],
                    'description'   => $gallery_value['description']
                );
            }
        }else{
            $gallery_page = '';
        }


        return view('beneficios-sociales-sustentabilidad', compact('title','banner','content', 'page_children', 'pagename', 'features', 'image_video','video_id', 'gallery_page'));
    }

    function page_parent($id){
        $id = wp_get_post_parent_id($id);
        $wp_query = new \WP_Query();
        $all_wp_pages = $wp_query->query(array('post_type' => 'page', 'posts_per_page' => -1));
        $page_children = get_page_children($id, $all_wp_pages);
        return $page_children;
    }
}