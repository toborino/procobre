<?php
namespace Theme\Controllers;
use Themosis\Route\BaseController;

class SearchController extends BaseController
{
    public function result(){

        $input_search = \Input::get('s');

        $query = new \WP_QUERY([
            'post_type'         =>  'publicacion',
            'posts_per_page'    => -1,
            's'             => $input_search
        ]);

        $post_search = [];
        if($query->have_posts()){
            while ($query->have_posts()){
                $query->the_post();

                $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];

                $type_cat = $termsType->name;

                $image_featured =  get_field('featured_image');
                if(!empty($image_featured)){
                    $image_featured = $image_featured['url'];
                }else{
                    $image_featured = '';
                }

                $short_description = get_field('short_description');

                $date = get_the_date();
                $new_date = explode('-', $date);
                $year = $this->dateYear();

                $post_search[] = array(
                    'title'         => get_the_title(),
                    'description'   => $short_description,
                    'image'         => $image_featured,
                    'type_cat'      => $type_cat ?? '',
                    'date'          => $new_date[2].' de '.$year[$new_date['1']].' del '.$new_date[0],
                    'link'          => get_the_permalink()
                );

            }
        }
        $count = count($post_search);

        $banner = get_field('banner_page_search', 'option');
        if(!empty($banner)){
            $banner = $banner['url'];
        }else{
            $banner;
        }

        return view('resultado-de-busqueda', compact('post_search', 'input_search', 'count', 'banner'));
    }

    function dateYear(){

        $year = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );

        return $year;

    }

}