<?php

namespace Theme\Controllers;


class TrensInnovationsController
{

    public function trens($post){
        global $wpdb;
        global $post;
        $post = get_post($post->ID);

        $title = $post->post_title;
        $description = $post->post_content;

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $post_trens = new \WP_Query(
            array(
                'post_type'  => 'trens-innovations',
                'showposts'  => -1
            )
        );

        $trens = '';
        if($post_trens->have_posts()){
            while ($post_trens->have_posts()){
                $post_trens->the_post();

                if(!empty(get_field('image_preview'))){
                    $image = get_field('image_preview')['url'];
                }else{
                    $image = '';
                }

                $trens[] = array(
                    'title'         => get_the_title(),
                    'description'   => get_field('short_description'),
                    'image'         => $image,
                    'link'          => get_the_permalink()
                );
            }
        }

        return view('template-trens-innovations', compact('title','description','banner', 'trens'));
    }

    public function single($post){

        $singleID  = $post->ID;
        $banner = get_field('banner');
        $title_single = get_the_title();
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }
        $content_image = get_field('content_image');
        if (!empty($content_image)) {
            $content_image = $content_image['url'];
        } else {
            $content_image = '';
        }


        $post_trens = new \WP_Query(
            array(
                'post_type'  => 'trens-innovations',
                'showposts'  => -1
            )
        );

        $single = get_post($post->ID);

        $current_slug = $single->post_name;

        $trens = '';
        if($post_trens->have_posts()){
            while ($post_trens->have_posts()){
                $post_trens->the_post();

                if(!empty(get_field('image_preview'))){
                    $image = get_field('image_preview')['url'];
                }else{
                    $image = '';
                }

                $trens[] = array(
                    'title'         => get_the_title(),
                    'slug'         => basename(get_permalink()),
                    'link'          => get_the_permalink()
                );
            }
        }

        $post_biblioteca = get_field('biblioteca', $singleID);
        

        if(!empty($post_biblioteca)){
            foreach ($post_biblioteca as $post_bilio){

                $file = get_field('file', $post_bilio->ID);
                if(!empty($file)){
                    $file = $file['url'];
                }else{
                    $file = '';
                }

                $termsType = get_the_terms( $post_bilio->ID, 'trens_cat' )[0];

                if(!empty($termsType)){
                    $type_cat = $termsType->name;
                }else{
                    $type_cat = '';
                }

                $biblioteca[] = array(
                    'category'  => $type_cat,
                    'title'     => $post_bilio->post_title,
                    'type'      => get_field('type', $post_bilio->ID),
                    'video'     => get_field('id_video', $post_bilio->ID),
                    'link'      => get_field('link', $post_bilio->ID),
                    'file'      => $file
                );
            }
        }



        $content_single = get_field('content', $post->ID);

        return view('single-trens', compact('banner', 'title_single', 'content_image', 'content_single','trens', 'current_slug', 'biblioteca'));
    }
}