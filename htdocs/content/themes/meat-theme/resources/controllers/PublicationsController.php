<?php
/**
 * User: charly1
 * Date: 01-12-17
 * Time: 15:36
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;



class PublicationsController extends BaseController
{

    public function publications($post){


        $title = get_the_title();

        $banner = get_field('banner');
        if (!empty($banner)) {
            $banner = get_field('banner')['url'];
        } else {
            $banner = '';
        }

        $content = get_the_content();


        $categories = get_terms( 'Categorias', array(
            'hide_empty' => 0,
        ) );


        return view('documentos', compact('title', 'banner','content','page_children', 'categories'));

    }

    public function CategoryPublications(){
        setlocale(LC_ALL,"es_CL");

        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        $term_slug = $term->slug;
        $term_name = $term->name;

        $link_cat = get_term_link($term);

        $image_id = get_term_meta($term->term_id, 'imagen', true );
        $image = wp_get_attachment_url( $image_id );

        $description = get_term_meta($term->term_id, 'contenido_interior', true );

        $image_id = get_term_meta($term->term_id, 'banner', true );
        $image_banner = wp_get_attachment_url( $image_id );

        $short_text =  $term->description;

        $categories = get_terms( 'Categorias', array(
            'hide_empty' => 0,
        ) );

        $cate_type = get_terms( 'cat_type', array(
            'hide_empty' => 0,
        ) );

        $post_publications = new \WP_Query(
            array(
                'post_type' => 'publicacion',
                'showposts' => 4,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'Categorias',
                        'field'    => 'slug',
                        'terms'    => $term_slug,
                    ),
                ),
            )
        );

        if($post_publications->have_posts()){
            while ($post_publications->have_posts()){
                $post_publications->the_post();

                $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];

                $type_cat = $termsType->name;

                $image_featured =  get_field('featured_image');
                if(!empty($image_featured)){
                    $image_featured = $image_featured['url'];
                }else{
                    $image_featured = '';
                }

                $short_description = get_field('short_description');

                $date = get_the_date();
                $new_date = explode('-', $date);
                $year = $this->dateYear();

                $publications[] = array(
                    'title'         => get_the_title(),
                    'description'   => $short_description,
                    'image'         => $image_featured,
                    'type_cat'      => $type_cat,
                    'date'          => $new_date[2].' de '.$year[$new_date['1']].' del '.$new_date[0],
                    'link'          => get_the_permalink()
                );
            }
        }else{
            $aplications = '';
        }

        $count_post = array(
            'post_type' => 'publicacion',
            'showposts' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'Categorias',
                    'field'    => 'slug',
                    'terms'    => $term_slug,
                ),
            ),
        );

        $count_publication = get_posts($count_post);

        return view('documento-detalle', compact('categories', 'term_slug', 'image_banner', 'short_text', 'term_name', 'image','description', 'publications', 'cate_type','link_cat','count_publication'));
    }

    public function SinglePublication(){

        $gallery = get_field('gallery');



        return view('documento-interior', compact('gallery'));
    }

    function dateYear(){

        $year = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );

        return $year;

    }
}