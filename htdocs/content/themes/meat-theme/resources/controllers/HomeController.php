<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;

class HomeController extends BaseController
{

    public function index()
    {
        if(get_field('slider')){

            foreach (get_field('slider') as $slider_home){
                $slider[] = array(
                    'image'         => $slider_home['image']['url'],
                    'title'         => $slider_home['title'],
                    'description'   => $slider_home['description'],
                    'link'          => $slider_home['link']
                );
            }

        }else{
            $slider = '';
        }

        $section_us = array(
            'image_us'      => get_field('image_us')['url'],
            'title_us'         => get_field('title_us'),
            'link_us'          => get_field('link_us'),
            'description_us' => get_field('description_us')
        );

        $section_cobre = array(
            'image_video'       => get_field('background_image')['url'],
            'title_video'       => get_field('title_video'),
            'link_video'        => get_field('link_externo'),
            'description_video' => get_field('description_video'),
            'id_video'          => get_field('video_id')
        );

        $section_benefits = array(
            'title_benefits'       => get_field('title_benefits'),
            'description_benefits' => get_field('description_benefits')
        );

        $section_aplications = array(
            'title_aplications'       => get_field('title_aplications'),
            'description_aplications' => get_field('description_aplications')
        );


        $post_trens = new \WP_Query(
            array(
                'post_type'  => 'trens-innovations',
                'showposts'  => -1
            )
        );

        $trens = '';
        if($post_trens->have_posts()){
            while ($post_trens->have_posts()){
                $post_trens->the_post();

                if(!empty(get_field('image_preview'))){
                    $image = get_field('image_preview')['url'];
                }else{
                    $image = '';
                }

                $icon = get_field('icono');
                if(!empty($icon)){
                    $icon =  $icon['url'];
                }else{
                    $icon = '';
                }

                $trens[] = array(
                    'title'         => get_the_title(),
                    'description'   => get_field('short_description'),
                    'image'         => $icon,
                    'link'          => get_the_permalink()
                );
            }
        }



        $post_aplications = new \WP_Query(
            array(
                'post_type' => 'aplications',
                'showposts' => '10'
            )
        );

        if($post_aplications->have_posts()){
            while ($post_aplications->have_posts()){
                $post_aplications->the_post();

                $aplications[] = array(
                    'title'        => get_the_title(),
                    'image'        => get_field('image_aplication')['url'],
                    'link'         => get_the_permalink()
                );
            }
        }else{
            $aplications = '';
        }

        $post_publications = new \WP_Query(
            array(
                'post_type' => 'publicacion',
                'showposts' => 3,

            )
        );

        if($post_publications->have_posts()){
            while ($post_publications->have_posts()){
                $post_publications->the_post();

                $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];
                if(!empty($termsType)) {
                    $type_cat = $termsType->name;
                }else{
                    $type_cat = '';
                }

                $image_featured =  get_field('featured_image');
                if(!empty($image_featured)){
                    $image_featured = $image_featured['url'];
                }else{
                    $image_featured = '';
                }

                $date = get_the_date();
                $new_date = explode('-', $date);
                $year = $this->dateYear();

                $short_description = get_field('short_description');

                $publications[] = array(
                    'title'         => get_the_title(),
                    'description'   => $short_description,
                    'image'         => $image_featured,
                    'type_cat'      => $type_cat,
                    'date'          => $new_date[2].' de '.$year[$new_date['1']].' del '.$new_date[0],
                    'link'          => get_the_permalink()
                );
            }
        }else{
            $publications = '';
        }

        return view('index', compact('slider', 'section_us', 'section_cobre', 'section_benefits', 'section_aplications', 'aplications', 'publications', 'trens'));
    }

    function dateYear(){

        $year = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );

        return $year;

    }
}
