<?php
/**
 * User: cGarcia
 * Date: 24-11-17
 * Time: 10:47
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;

class UsController extends BaseController
{

    public function us($post){

        $banner = get_field('banner', $post->ID);
        if(!empty($banner)){
            $banner = $banner['url'];
        }

        $description = array(
            'title'         => get_field('title_description', $post->ID),
            'content'       => get_field('content_description', $post->ID),
            'image_content' => get_field('image_content', $post->ID)['url']
        );

        $vision = array(
            'title'     => get_field('title_vision', $post->ID),
            'content' => get_field('content_vision', $post->ID)
        );

        $mision = array(
            'title'     => get_field('title_mision', $post->ID),
            'content'   => get_field('content_mision', $post->ID),
            'features'  => get_field('features', $post->ID)
        );

        $propuesta = array(
            'title'     => get_field('title_propuesta', $post->ID),
            'content'   => get_field('content_propuesta', $post->ID),
            'file'      => get_field('file_propuesta')['url']
        );

        $offices = array(
            'title'     => get_field('title_ica', $post->ID),
            'content'   => get_field('content_ica', $post->ID),
            'Offices'   => get_field('Offices', $post->ID)
        );

        return view('nosotros', compact('banner', 'description', 'vision', 'mision', 'propuesta','offices'));

    }

}