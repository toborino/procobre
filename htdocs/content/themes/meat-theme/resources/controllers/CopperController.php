<?php
/**
 * User: cGarcia
 * Date: 21-11-17
 * Time: 17:26
 */

namespace Theme\Controllers;
use Themosis\Route\BaseController;

class   CopperController extends BaseController
{
    public function Copper($post){
        global $post;
        $post = get_post($post->ID);
        $banner  = get_field('banner');
        if(!empty($banner)){
            $banner  = get_field('banner')['url'];
        }else{
            $banner = '';
        }
        $title = get_the_title();
        $content = $post->post_content;

        $wp_query = new \WP_Query();
        $all_wp_pages = $wp_query->query(array('post_type' => 'page', 'posts_per_page' => -1));
        $page_children = get_page_children($post->ID, $all_wp_pages);

        return view('el-cobre', compact('banner', 'title', 'content', 'page_children' ));
    }

    public function pagesCopper($post){
        global $post;
        $post = get_post($post->ID);
        $id_parent = wp_get_post_parent_id($post->ID);
        $wp_query = new \WP_Query();
        $all_wp_pages = $wp_query->query(array('post_type' => 'page', 'posts_per_page' => -1));
        $page_children = get_page_children($id_parent, $all_wp_pages);

        $banner  = get_field('banner');
        if(!empty($banner)){
            $banner  = get_field('banner')['url'];
        }else{
            $banner = '';
        }

        $pagename = get_query_var('pagename');
        if ( !$pagename && $id > 0 ) {
            $post = $wp_query->get_queried_object();
            $pagename = $post->post_name;
        }

        $content = $post->post_content;

        $image_video  = get_field('image_sub');
        if(!empty($image_video)){
            $image_video  = $image_video['url'];
        }else{
            $image_video = '';
        }

        $title = get_the_title();

        $video_id = get_field('video_id');


        return view('el-cobre-subpage', compact('banner','page_children', 'pagename', 'image_video', 'video_id','title', 'content'));
    }
}