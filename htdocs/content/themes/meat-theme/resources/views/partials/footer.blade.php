<footer class="footer">
	<div class="container">
		<div class="row margin-top hidden-xs">
			<div class="col-lg-4 col-sm-6 col-xs-6 hidden-xs">
				<img class="logo-copper" src="{{ get_bloginfo('template_url') }}/dist/images/logotipo-copper-alliance.svg" alt=""/>
			</div>
			@if(get_field('rrss', 'option'))
				<div class="col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-2 col-xs-6 hidden-xs">
					<div class="content-rrss">
						@foreach(get_field('rrss', 'option') as $rrss)
						<a href="{{ $rrss['link'] }}" rel="nofollow"><i class="fa {{ $rrss['rrss'] }}" aria-hidden="true"></i></a>
						@endforeach
					</div>
				</div>
			@endif
		</div>
		<div class="row">

			@if(!empty(get_field('menu_footer', 'option')))
				@php $cont = 1; @endphp
				@foreach(get_field('menu_footer', 'option') as $menus)
					@if($cont == 1 )
						<div class="col-lg-4 col-sm-4 hidden-xs">
							<ul>
					@endif
							<li><a href="{{ Loop::link($menus->ID) }}">{{ $menus->post_title }}</a></li>
					@if($cont == 4 )
							</ul>
						</div>
						@php $cont = 0 @endphp

					@endif
					@php  $cont ++; @endphp
				@endforeach
			@endif
		</div>
		<div itemscope itemtype="http://data-vocabulary.org/LocalBusiness">
			<div xmlns:v="http://rdf.data-vocabulary.org/#">
			<div class="col-lg-4 col-sm-4 hidden-xs hidden-sm">
				<div class="international-copper">
					<div itemprop="name">{!! get_field('oficinas', 'option') !!}</div>
					<i itemprop="telephone" class="fa fa-phone" aria-hidden="true"><span>{{ get_field('telefono', 'option') }}</span></i>
					<i itemprop="address" class="fa fa-map-marker" aria-hidden="true"><span class="set-span">{{ get_field('direccion', 'option') }}</span></i>
				</div>
			</div>
			<div class="col-lg-4 col-sm-4 hidden-lg hidden-md">
				<div class="international-copper">
					{!! get_field('oficinas', 'option') !!}
					<p itemprop="telephone" class="texto-icon texto-icon-1">{{ get_field('telefono', 'option') }}</p>
					<p itemprop="address"  class="texto-icon texto-icon-2">{{ get_field('direccion', 'option') }}</p>
				</div>
			</div>
			<div class="copyright">
				<p class="copyright-text">COPYRIGHT © 2017 ICA</p>
			</div>
			</div>
		</div>
	</div>
</footer>