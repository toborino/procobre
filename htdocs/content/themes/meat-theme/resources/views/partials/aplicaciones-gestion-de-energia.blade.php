@extends('layout/default')
@section('content')
@include('components/small-menu-top')
<div class="el-cobre">
	@include('components/banner-default-pages' , ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "beneficios sociales", 'backgroundImage' => '/images/copper-alliance--aplicaciones.png','setTitle' => ''])
	<div class="container body-aplicaciones">
		<div class="col-lg-4 col-sm-5 section-relative">
			@include('components/aplicaciones-sidebar')
		</div>
		<div class="col-lg-8 col-sm-7 section-relative">
			@include('components/aplicaciones-body', ['title' => 'gestión de energía'])
		</div>
	</div>
</div>
@endsection