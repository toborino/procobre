<div id="headroom-menu">
	<div class="content-menu-desktop">
		@php $user = User::current() @endphp
		@include('components/search')
		@if ( is_user_logged_in() )
			@include('components/small-menu-top-user',['display_name' => $user->display_name])
		@else
			@include('components/small-menu-top')
		@endif
		<div class="section-menu-desktop">
			<div id="menu-desktop">
				<div class="logo">
					<a href="/"><img src="{{ get_bloginfo('template_url') }}/dist/images/logotipo-copper-alliance.svg" alt=""/></a>
				</div>
				<ul>
					{{ wp_nav_menu_top() }}
					<li class=""><a href="/contacto" class="btn-contacto">CONTACTO</a></li>
				</ul>
			</div>
		</div>
		<div class="section-menu-desktop-small">
			<div class="container">
				<div class="row">
					<div class="logo">
						<a href="/"><img src="{{ get_bloginfo('template_url') }}/dist/images/logo-copper-dark.svg" alt=""/></a>
					</div>
					<ul>
						{{ wp_nav_menu_top() }}
						<li><a href="/contacto">CONTACTO</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


@include('components/menu-mobile')

<div id="menu-scroll">

</div>

