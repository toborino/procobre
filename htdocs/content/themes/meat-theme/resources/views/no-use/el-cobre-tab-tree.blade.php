<div class="el-cobre-interior-content">
	<h1 class="animate">ALEACIONES</h1>
	<hr class="line animate">
</div>
<div class="el-cobre-body set-el-cobre-body-tree animate">
	<p>El cobre tiene aplicaciones casi infinitas en la industria debido a su posibilidad de combinarse con otros elementos y dar lugar a aleaciones con características diferentes. No hay otro metal que presente tantas coloraciones como el cobre y sus combinaciones: rojo, oro, marrón, verde, gris.</p>
	<p>El cobre es esencial a la vida moderna: proporciona electricidad y agua potable en nuestras casas y ciudades y ofrece un apoyo fundamental para el desarrollo sostenible.</p>
​​​​​​​
	<p>Hay 400 combinaciones de aleaciones de cobre, cada una con una combinación única de propiedades de acuerdo a sus aplicaciones y procesos de fabricación y entornos.</p>
</div>
<div class="content-image animate" style="background-image: url('{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-random-2.png');">
	 <img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-random-2.png" alt="">
</div>
<div class="content-list-text animate">
	<ul class="set-ul-top">
		<li><span>Cobre Niquel:</span>Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur. Solidumque nabataeaque moles. Ignea umentia persidaque radiis fabricator subsidere umentia prima adhuc.</li>
		<li><span>Berilio:</span>Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur. Solidumque nabataeaque moles. Ignea umentia persidaque radiis fabricator subsidere umentia prima adhuc.</li>
		<li><span>Niquel y Plata:</span>Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur. Solidumque nabataeaque moles. Ignea umentia persidaque radiis fabricator subsidere umentia prima adhuc.</li>
		<li><span>Bronce de cañon:</span>Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur. Solidumque nabataeaque moles. Ignea umentia persidaque radiis fabricator subsidere umentia prima adhuc.</li>
		<li><span>Cobre y estaño:</span>Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur. Solidumque nabataeaque moles. Ignea umentia persidaque radiis fabricator subsidere umentia prima adhuc.</li>
		<li><span>El bronce:</span>Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur. Solidumque nabataeaque moles. Ignea umentia persidaque radiis fabricator subsidere umentia prima adhuc.</li>
	</ul>
</div>
@include('components/block-shares')