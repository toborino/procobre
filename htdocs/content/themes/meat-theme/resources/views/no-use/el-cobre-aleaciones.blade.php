@extends('layout.default')
@section('content')
@include('components/small-menu-top')
<div class="el-cobre">
	@include('components/banner-default-aplicaciones', ['textBanner' => '', 'breadcrumb' => '', 'mainTitle' => "El cobre", 'backgroundImage' => '/images/copper-alliance--el-cobre.png','setTitle' => ''])
	<div class="el-cobre-interior-body">
		<div class="nav nav-tabs el-cobre-interior-body-filter">
			<a href="/el-cobre-historia"><button class="button-tabs">HISTORIA DEL COBRE</button></a>
			<a href="/el-cobre-propiedades"><button class="button-tabs">PROPIEDADES</button></a>
			<a href="/el-cobre-aleaciones"><button class="button-tabs active">ALEACIONES</button></a>
		</div>
	</div>
	<div class="tab-content">
		<div id="menu3" class="default-height-tabs">
			@include('no-use.el-cobre-tab-tree')
		</div>
	</div>
</div>
@endsection