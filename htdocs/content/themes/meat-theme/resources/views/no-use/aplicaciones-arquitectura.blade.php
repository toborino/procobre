@extends('layout/default')
@section('content')
@include('components/small-menu-top')
<div class="el-cobre">
	@include('components/banner-default-aplicaciones' , ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "beneficios sociales", 'backgroundImage' => '/images/copper-alliance--aplicaciones.png','setTitle' => ''])
	@include('components/aplicaciones-sidebar-mobile')
	<div class="container body-aplicaciones">
		<div class="col-lg-4 col-sm-0 section-relative hidden-sm">
			@include('components/aplicaciones-sidebar')
		</div>
		<div class="col-lg-8 col-sm-12 section-relative">
			@include('components/aplicaciones-body', ['title' => 'arquitectura'])
		</div>
	</div>
</div>
@endsection