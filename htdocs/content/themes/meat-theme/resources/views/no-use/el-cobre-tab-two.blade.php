<div class="el-cobre-interior-content">
	<h1 class="animate">PROPIEDADES DEL COBRE</h1>
	<hr class="line animate">
</div>
<div class="content-image animate" style="background-image: url('{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-random-1.png');">
	 <img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-random-1.png" alt="">
</div>
<div class="el-cobre-body set-el-cobre-body-two animate">
	<p>El cobre puede ser visto de varias formas. Además de elemento esencial para la vida, presenta características únicas que lo hacen uno de los metales que más beneficios traen para la humanidad.<br>
	Cada año, unos 25 millones de toneladas de productos de cobre se suministran en todo el planeta a través de una 	cadena de abastecimiento sofisticada. <br><br>

	No siempre podemos verlo, el cobre se oculta detrás de las paredes o en dentro de los equipos, cubierto por aislamiento de protección. Pero casi todos los momentos de nuestra vida se ve afectada por los beneficios que ofrecen los productos de cobre.</p>
	<p>El cobre es esencial a la vida moderna: proporciona electricidad y agua potable en nuestras casas y ciudades y ofrece un apoyo fundamental para el desarrollo sostenible.</p>
​​​​​​​
	<p>La capacidad de conductividad eléctrica y térmica del cobre ayuda a proteger el medio ambiente a través de la eficiencia energética, pues reduce las emisiones de gases nocivos de efecto invernadero.
	En los cuidados con la salud, su propiedad antimicrobiana ha demostrado una reducción en el riesgo de infecciones del orden de 58%.<br>
	Finalmente, el cobre es un elemento esencial para todas las formas de vida.</p>
</div>
<div class="content-list-text animate">
	<ul>
		<li><span>Conductividad eléctrica:</span> la generación, transmisión y uso de la electricidad transformaron el mundo moderno. Y el cobre ha desempeñado un papel importante en estos cambios debido a su óptima capacidad de conducción eléctrica, ampliamente utilizado en la composición de alambres, cables y otros componentes eléctricos.</li>
		<li><span>Conductividad térmica:</span>el cobre es un excelente conductor de calor, alrededor de 30 veces más que el acero y casi el doble del aluminio. Por lo tanto, es adecuado para aplicaciones que requieren una rápida transferencia de calor, tales como intercambiadores de calor, radiadores, aparatos de aire acondicionado, computadoras y televisores.</li>
		<li><span>Flexibilidad y maleabilidad:</span>por el hecho de ser fácilmente soldado, pegado o fundido, el cobre se utiliza ampliamente en la industria, especialmente en aplicaciones hidráulicas, pero también en arquitectura y en objetos de diseño.</li>
		<li><span>Tensión:</span>las aleaciones de cobre son esencialmente reforzadas mecánicamente por trabajo en frío o agregados de solución sólida, lo que mejora el endurecimiento. En el estado recocido, el límite convencional de elasticidad y la resistencia a la tensión varía inversamente con el tamaño del grano.</li>
		<li><span>Colores:</span>sus tonos naturalmente metálicos varían del rojo al amarillo, pasando por verdes y grises. Una serie de otros colores puede ser obtenida por medio del tratamiento de su superficie por medios químicos o electroquímicos.</li>
		<li><span>Corrosión:</span>el cobre y sus aleaciones son extremadamente resistentes a la corrosión atmosférica, pero con el tiempo puede formar una decoloración superficial o una película manchada. El espesor y la composición química de esta capa varían en función del tiempo de exposición, de las condiciones atmosféricas y de la química de la aleación de base.</li>
		<li><span>Antimicrobiano:</span>esta propiedad es intrínseca y ha sido explotado durante siglos. Pero las últimas pruebas realizadas por laboratorios independientes llevaron a que la Agencia de Protección Ambiental registrase el cobre y varias de sus aleaciones, por su capacidad para eliminar diversos patógenos en poco tiempo.</li>
	</ul>
</div>
@include('components/block-shares')