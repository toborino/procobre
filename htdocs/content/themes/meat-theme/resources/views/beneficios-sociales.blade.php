@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => $title, 'backgroundImage' => $banner ,'setTitle' => ''])
	<div class="body-beneficios-sociales">
		<div class="beneficios-sociales-text animate">
			{!! $short_description !!}
		</div>
		<div class="content-target">
			<div class="content-target-el-cobre">
				@if(!empty($page_children))
					@foreach($page_children as $benefits)
						<div class="target-el-cobre animate">
							<div class="target-el-cobre-image" style="background-image: url('{{ get_field('image_preview', $benefits->ID)['url'] }}');">
								<img src="{{ get_field('image_preview', $benefits->ID)['url'] }}" alt="">
							</div>
							<div class="target-el-cobre-text">
								<h2>{{ $benefits->post_title }}</h2>
								<div class="content-text" data-equalize="target-el-cobre-text">
								{!! get_field('short_description', $benefits->ID) !!}
								</div>
								<a href="{{ Loop::link($benefits->ID) }}" class="button super-button"><span>VER MÁS</span></a>
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</div>
		<div class="beneficios-sociales-body-text">
			<div class="content-text animate content-list-text">
				{!!  apply_filters('the_content', $content); !!}
			</div>
		</div>
	</div>
</div>
@endsection