@extends('layout/default')
@section('content')
<div class="el-cobre">
	<div class="signin">
		<div class="login">
			<form class="form-signin">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="ca-email">EMAIL :</label>
	    			<input type="email" class="form-control input-ca" id="ca-email" aria-describedby="emailHelp" placeholder="Ingrese su email">
					</div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<label class="set-margin-label" for="ca-password">CONTRASEÑA :</label>
	    			<input type="password" class="form-control input-ca" id="ca-password" aria-describedby="emailHelp" placeholder="Ingrese su contraseña">
					</div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<button class="button-contact">ENVIAR</button>
					</div>
				</div>
			</form>
			<div class="col-lg-12 content-other-button">
				<div class="form-group">
					<a href="#">Olvidaste tu contraseña</a>
					<a href="#">Solicitar acceso</a>
				</div>
			</div>
			<a href="/copper-alliance-website" class="close-signin"><i class="fa fa-times" aria-hidden="true"></i></a>
		</div>
	</div>
</div>
@endsection