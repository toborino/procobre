@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => $content, 'breadcrumb' => 'breadcrumb-out','mainTitle' => $title, 'backgroundImage' => $banner,'setTitle' => 'set-title-and-text'])
	<div class="documents-body content-miembros">

		<div class="documents-menu">
			<a href="#" class="documents-button documents-button-1 active ">Noticias</a>
			<a href="/documentos" class="documents-button documents-button-1 ">Documentos</a>
			<a href="/videos" class="documents-button documents-button-1">Video</a>
		</div>

		<div class="top-50"></div>
		<div class="content-miembros-asociado-body">
			<div class="content-miembros-body">

				<div class="target-documento-detalle animate">
					<div class="detalle-imagen">
						<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-1.jpg" alt="">
					</div>
					<div class="detalle-text">
						<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
						<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
					</div>
					<div class="detalle-button">
						<a href="#" class="button super-button"><span>DESCARGAR</span></a>
					</div>
				</div>
				<div class="target-documento-detalle animate">
					<div class="detalle-imagen">
						<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-1.jpg" alt="">
					</div>
					<div class="detalle-text">
						<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
						<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
					</div>
					<div class="detalle-button">
						<a href="#" class="button super-button"><span>DESCARGAR</span></a>
					</div>
				</div>

				<div class="target-documento-detalle animate">
					<div class="detalle-imagen">
						<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-2.jpeg" alt="">
					</div>
					<div class="detalle-text">
						<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
						<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
					</div>
					<div class="detalle-button">
						<a href="#" class="button super-button"><span>DESCARGAR</span></a>
					</div>
				</div>

				<div class="target-documento-detalle animate">
					<div class="detalle-imagen">
						<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-4.jpg" alt="">
					</div>
					<div class="detalle-text">
						<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
						<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
					</div>
					<div class="detalle-button">
						<a href="#" class="button super-button"><span>DESCARGAR</span></a>
					</div>
				</div>

			</div>
			<div class="content-paginator animate">
				@include('components/paginator')
			</div>
		</div>
	</div>
</div>
@endsection