@extends('layout/default')
@section('content')
    <div class="el-cobre">
        @include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => $title, 'backgroundImage' => $banner ,'setTitle' => ''])
        <div class="body-beneficios-sociales">
            <div class="beneficios-sociales-text animate">
               <p> {!! $description !!} </p>
            </div>
            <div class="content-target">
                <div class="content-target-el-cobre">
                    @if(!empty($trens))
                        @foreach($trens as $post_trens)
                            <div class="target-el-cobre animate">
                                <div class="target-el-cobre-image" style="background-image: url('{{ $post_trens['image'] }}');">
                                    <img src="{{ $post_trens['image'] }}" alt="{{ $post_trens['title'] }}">
                                </div>
                                <div class="target-el-cobre-text">
                                    <h2 data-equalize="target-el-cobre-title">{{ $post_trens['title'] }}</h2>
                                    <div class="content-text" data-equalize="target-el-cobre-text">
                                        {!! $post_trens['description'] !!}
                                    </div>
                                    <a href="{{ $post_trens['link'] }}" class="button super-button"><span>VER MÁS</span></a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection