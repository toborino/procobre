@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => $content, 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => $title, 'backgroundImage' => $banner,'setTitle' => 'set-title-and-text'])
	<div class="documents-body content-miembros">

		<!--<div class="documents-menu">
			<a href="#" class="documents-button documents-button-1 active ">Noticias</a>
			<a href="/documentos" class="documents-button documents-button-1 ">Documentos</a>
			<a href="/videos" class="documents-button documents-button-1">Video</a>
		</div>-->

		 <!-- @ include('components/filter-pages', ['term_slug' => 'slug', 'link_cat'  => 'link']) -->
		<div class="top-50"></div>
		@if(!empty($publications))
			<div class="content-miembros-body">
				@foreach($publications as $publication)
					@include('components/target-articulos', [
						'title' 		=> $publication['title'],
						'description' 	=> $publication['description'],
						'image'			=> $publication['image'],
						'type_cat'		=> $publication['type_cat'],
						'date'			=> $publication['date'],
						'link'			=> $publication['link']
						])
				@endforeach
			</div>
		@endif
		<!--<div class="content-paginator animate">
			@ include('components/paginator')
		</div>-->
	</div>
</div>
@endsection