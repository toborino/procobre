@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => 'Addidit pinus spisso aetas freta sinistra scythiam obliquis. Grandia et tempora ne ipsa indigestaque recessit. Semina quae tellus amphitrite obsistitur ita conversa. Fronde principio eurus! Derecti homo. Nondum habendum cuncta obsistitur solidumque.', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "Videos", 'backgroundImage' => '/images/copper-alliance--artículos.png','setTitle' => 'set-title-and-text'])
	<div class="documents-body content-miembros">
		<div class="content-miembros-filter animate">
			@include('components/filter-pages', ['term_slug' => 'slug', 'link_cat'  => 'link'])
	</div>
</div>
<div class="content-miembros-asociado-body content-miembro-video">
	<div class="documents-body set-content-miembros-top">
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-1.jpg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-2.jpeg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-3.jpeg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-4.jpg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-1.jpg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-2.jpeg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-3.jpeg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
		<div class="target-documento-detalle target-video animate">
			<div class="detalle-imagen">
				<img src="{{ get_bloginfo('template_url') }}/dist/images/copper-alliance-article-4.jpg" alt="">
				@include('components/button')
			</div>
			<div class="detalle-text">
				<i class="fa fa-file-video-o" aria-hidden="true"></i>
				<h3 class="title">COBRE: SALUD, MEDIO AMBIENTE Y NUEVAS...</h3>
			</div>
			<div class="detalle-button">
				<a href="#" class="button super-button"><span>DESCARGAR</span></a>
			</div>
		</div>
	</div>
	<div class="content-paginator animate">
		@include('components/paginator')
	</div>
</div>
@endsection


