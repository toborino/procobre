@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "resultado de búsqueda", 'backgroundImage' => $banner,'setTitle' => ''])
	<div class="documents-body content-miembros content-resultado-busqueda">
		<div class="info">
			<h3>HAY {{ $count }} RESULTADOS PARA LA BÚSQUEDA <span>"{{ $input_search }}"</span></h3>
		</div>
		@if(!empty($post_search))
			<div class="content-miembros-body">
				@foreach($post_search as $result)
				@include('components/target-resultado',[
					'title' 		=> $result['title'],
					'description'	=> $result['description'],
					'image' 		=> $result['image'],
					'link'			=> $result['link']
				])
				@endforeach
			</div>
		@endif
		<!--<div class="content-paginator animate">
			@include('components/paginator')
		</div>-->
	</div>
</div>
@endsection