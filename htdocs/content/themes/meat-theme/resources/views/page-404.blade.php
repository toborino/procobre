@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-page-error', [
		'textBanner' => '',
		'breadcrumb' => 'breadcrumb-out', 
		'mainTitle' => "ESTA PÁGINA NO EXISTE", 
		'backgroundImage' => '/images/404.jpg',
		'setTitle' => ''])
	<button class="button button-resultado"><a href="/">Ir al inicio</a></button>
</div>
@endsection