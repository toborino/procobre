<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	@wp_head

    <noscript>
        <style>
            #loading {
            	display: none !important
            }

            .animate {
            	visibility: visible !important; 
            }
        </style>
    </noscript>
</head>
<body>
	@include('components/loading')
	<div id="wrapper">
		@include('partials.header')
			@yield("content")
		@include('partials.footer')
	</div>
	<div id="youtube">
    <button class="youtube-close"><i class="fa fa-times"></i></button>
    <div class="overview"></div>
  </div>
	@wp_footer
</body>
</html>