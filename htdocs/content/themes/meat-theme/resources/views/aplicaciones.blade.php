<?php
if(is_page(Loop::id())) :

    $args = array(
        'posts_per_page' => 1,
        'post_type' => 'aplications'
    );

    /** Get the posts that match and grab the URL of the first post */
    $posts = get_posts($args);
    $redirect_url = get_permalink($posts[0]->ID);

    /** Redirect to the specified URL */
    wp_redirect($redirect_url);
    exit;

endif;
?>