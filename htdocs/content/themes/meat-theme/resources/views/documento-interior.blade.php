@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => get_the_title(), 'backgroundImage' => get_field('banner')["url"],'setTitle' => 'set-title'])
	<div class="documents-body">
		<div class="documents-menu-interior animate">
			<div class="volver">

				<a href="javascript:history.back(1)">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
				<p>VOLVER</p>
				</a>
			</div>
			<?php
            $termsCategory = get_the_terms( get_the_ID(), 'Categorias' )[0];
            $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];

            $type_cat = $termsType->name;
            $category = $termsCategory->name;
			?>
			<div class="content-info"><p>CATEGORÍA: {{ strtoupper($category) }}</p></div>
			<div class="content-info"><p>FECHA:  {{ get_the_date() }}</p></div>
			<div class="content-info"><p>TIPO: {{ strtoupper($type_cat) }}</p></div>
		</div>
		<div class="documents-interior-body">
			<div class="col-lg-8">

				<div class="body-text-documentos">
				{!! get_field('contenido') !!}
				</div>

				@if(get_field('image_video'))
					<div class="content-video" style="background-image: url('{{ get_field('image_video')['url']  }}')">
						<img style="visibility: initial !important;" src="{{ get_field('image_video')['url']  }}" alt="{{ get_the_title() }}">
						<div class="body-video">

							@include('components/button', ['video_id' => get_field('id_video')])
						</div>
						<div class="trama">
						</div>
					</div>
				@endif
				<br>
				<br>
				@if(!empty($gallery))
					<div class="imagen-carousel animate">
						<ul class="documento-interio-carousel">
							@foreach($gallery as $image)
								<li><img src="{{ $image['url'] }}" alt="{{ get_the_title() }}"></li>
							@endforeach
						</ul>
					</div>
					<div class="slider-nav animate">
						@foreach($gallery as $image)
							<img src="{{ $image['url'] }}" alt="{{ get_the_title() }}">
						@endforeach
					</div>
				@endif
				@include('components/block-shares')
			</div>
			@if(!empty(get_field('documento_pdf')))
				<div class="col-lg-4">
					<div class="content-dowloads animate">
						<h3 class="title">descargables</h3>
					</div>
					<div class="box-descagables animate">
						<div class="box-descagables-icon-text">
								<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
								<p>{{ get_the_title() }}</p>
						</div>
						<div class="box-descagables-icon-button">
							<a href="{{ get_field('documento_pdf')['url'] }}" target="_blank" class="button super-button"><span>DESCARGAR ARCHIVO</span></a>
						</div>
					</div>

				</div>
			@endif
		</div>
	</div>
</div>
@endsection