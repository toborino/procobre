@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => '', 'mainTitle' => "beneficios sociales", 'backgroundImage' => $banner,'setTitle' => ''])

	@if(!empty($page_children))
		<div class="el-cobre-interior-body">
			<div class="nav nav-tabs el-cobre-interior-body-filter">
				@foreach($page_children as $benefits)
					<a href="{{ Loop::link($benefits->ID) }}"><button class="button-tabs {{ $pagename == $benefits->post_name ? 'active' : '' }}">{{ $benefits->post_title }}</button></a>
				@endforeach
			</div>
		</div>
	@endif


		<div class="tab-content">
		<div id="menu3" class="default-height-tabs">
			<div class="el-cobre-interior-content">
				<h1 class="animate">{{ $title }}</h1>
				<hr class="line animate">
			</div>
			<div class="el-cobre-body set-el-cobre-body-tree content-list-text">
				{!!  apply_filters('the_content', $content); !!}
			</div>

			<div class="content-videos">
				<div class="el-cobre-interior-content">
					<h2 class="title animate">SUSTENTABILIDAD</h2>
					<hr class="line animate">
				</div>


				@if(!empty($gallery_page))
					@foreach($gallery_page as $gallery)
						<div class="content-videos-body animate">
							<h2>{{ $gallery['title'] }}</h2>
							<div class="video">
								<img src="{{ $gallery['image'] }}" alt="">
								@include('components/button',['video_id'=>$gallery['video_id']])
							</div>
							<div class="bockquote-video">
								<blockquote>
									{!!   $gallery['description'] !!}
								</blockquote>
							</div>
						</div>
					@endforeach
				@endif
				@include('components/block-shares')
			</div>
		</div>
	</div>
</div>
@endsection