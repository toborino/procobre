@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => '', 'mainTitle' => "beneficios sociales", 'backgroundImage' => $banner ,'setTitle' => ''])
	@if(!empty($page_children))
		<div class="el-cobre-interior-body">
			<div class="nav nav-tabs el-cobre-interior-body-filter">
				@foreach($page_children as $benefits)
					<a href="{{ Loop::link($benefits->ID) }}"><button class="button-tabs {{ $pagename == $benefits->post_name ? 'active' : '' }}">{{ $benefits->post_title }}</button></a>
				@endforeach
			</div>
		</div>
	@endif
	<div class="tab-content">
		<div id="menu1" >
			<div class="el-cobre-interior-content">
				<h1 class="animate">{{ $title }}</h1>
				<hr class="line animate">
			</div>
			<div class="el-cobre-body set-el-cobre-body-tree">
				{!!  apply_filters('the_content', $content); !!}
			</div>
			@include('components/block-shares')
		</div>
	</div>
</div>
@endsection