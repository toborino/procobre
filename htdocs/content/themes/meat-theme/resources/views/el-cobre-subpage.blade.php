@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-aplicaciones', ['textBanner' => '', 'breadcrumb' => '','mainTitle' => "El cobre", 'backgroundImage' => $banner ,'setTitle' => ''])
	@if(!empty($page_children))
		<div class="el-cobre-interior-body">
			<div class="nav nav-tabs el-cobre-interior-body-filter">
				@foreach($page_children as $sub_page)
					<a href="{{ Loop::link($sub_page->ID) }}"><button class="button-tabs {{ $pagename == $sub_page->post_name ? 'active' : ''  }}">{{ $sub_page->post_title }}</button></a>
				@endforeach
			</div>
		</div>
	@endif
	<div class="tab-content">
		<div id="menu1" class="default-height-tabs active">
			@include('components/el-cobre-tab-content',[
				'image_video'	=> $image_video,
				'video_id'	  	=> $video_id,
				'title'			=> $title,
				'content'	  	=> $content
				]

			)
		</div>	
	</div>
</div>
@endsection