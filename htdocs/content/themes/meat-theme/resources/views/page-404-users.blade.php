@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "ESTA PÁGINA NO EXISTE", 'backgroundImage' => '/images/copper-alliance-random-0.png','setTitle' => ''])
	<div class="content-buttons-404">
		<a class="button-inicio">Inicio</a>
		<a class="button-volver">Revisa nuestros documentos</a>
	</div>
</div>
@endsection