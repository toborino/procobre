@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', [
		'textBanner' => '',
		'breadcrumb' => 'breadcrumb-out',
		'mainTitle' => $title,
		'backgroundImage' => $banner,
		'setTitle' => '']
	)
	<div class="el-cobre-body">
		{!!   apply_filters('the_content', $content); !!}
	</div>

	<div class="el-cobre-target">
		<div class="container">
			<div class="content-target-el-cobre">
				@if(!empty($page_children))
					@foreach($page_children as $pages)
						@include('components/target-el-cobre', [
							'image' 			=> get_field('image_preview', $pages->ID)['url'],
							'title' 			=> 	$pages->post_title,
							'short_description' => get_field('short_description', $pages->ID),
							'link'				=> Loop::link($pages->ID)
							]
						)
					@endforeach
				@endif
			</div>
		</div>
	</div>

	<section style="display: block;">
		<div class="container">
			@include('components/target-info')
		</div>
	</section>
</div> 
@endsection