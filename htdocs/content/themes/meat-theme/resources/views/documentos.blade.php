@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => $title, 'backgroundImage' => $banner,'setTitle' => ''])
	<div class="container body-aplicaciones">
		<div class="el-cobre-body set-text-down">
		<p class="text-826252 animate">
			{!! $content !!}
		</p>
		</div>
	</div>


	<div class="el-cobre-target">
		<div class="container">
			<div class="content-target-el-cobre content-target-el-cobre-position">

				@if($categories)
					@foreach($categories as $category)
						<?php
						$image_id = get_term_meta($category->term_id, 'imagen', true );
                        $image_attributes = wp_get_attachment_url( $image_id );
						?>
						<div class="target-el-cobre animate">
							<div class="target-el-cobre-image" style="background-image: url('{{ $image_attributes }}');">
								<img src="{{ $image_attributes }}" alt="{{ $category->name }}">
							</div>
							<div class="target-el-cobre-text">
								<h2>{{ $category->name }}</h2>
								<p data-equalize="target-el-cobre-text">{{ $category->description }}</p>
								<a href="{{ get_term_link($category) }}" class="button super-button"><span>VER MÁS</span></a>
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
</div>
@endsection