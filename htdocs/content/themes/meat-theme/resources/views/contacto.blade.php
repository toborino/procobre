@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'textBanner', '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "Contacto", 'backgroundImage' => $banner,'setTitle' => ''])
	<div class="container content-contact">
		<div class="col-lg-8 col-lg-offset-2 set-padding">  
			<div class="bajada-text animate">{!! $content !!}</div>
			<form id="form" class="form-ajax form-validate" action="{{ admin_url( 'admin-ajax.php' ) }}" method="post">
			  	<input type="hidden" name="action" value="form_contact">
				<input type="hidden" name="email_destino" value="{{ get_field('email_destino') }}">
				<div class="col-lg-6 animate">
					<div class="form-group">
						<label for="inputNombre">NOMBRE :</label>
	    			<input type="text" class="form-control input-ca" name="name" id="inputNombre" placeholder="Ingrese su nombre" data-msg="Ingrese un nombre" data-require="Campo requerido">
					</div>
				</div>
				<div class="col-lg-6 animate">
					<div class="form-group">
						<div class="form-group">
							<label for="inputApellido">APELLIDOS :</label>
		    			<input type="text" class="form-control input-ca" name="lastname" id="inputApellido" placeholder="Ingrese su apellido" data-msg="Ingrese sus apellidos" data-require="Campo requerido">
						</div>
					</div>
				</div>
				<div class="col-lg-6 animate">
					<div class="form-group">
						<div class="form-group">
							<label for="inputEmail">EMAIL :</label>
		    			<input type="text" class="form-control input-ca" name="email" id="inputEmail" placeholder="ingrese su email" data-msg="Ingrese un email" data-require="Campo requerido">
						</div>
					</div>
				</div>
				<div class="col-lg-6 animate">
					<div class="form-group">
						<div class="form-group">
							<label for="inputAsunto">ASUNTO :</label>
		    			<input type="text" class="form-control input-ca" name="subject" id="inputAsunto" placeholder="Ingrese un asunto" data-msg="Ingrese un asunto" data-require="Campo requerido">
						</div>
					</div>
				</div>
				<div class="col-lg-12 animate">
					<div class="form-group">
						<label for="inputMensaje">MENSAJE :</label>
	    			<textarea type="text" class="form-control input-ca" name="messages" id="inputMensaje"  placeholder="Ingrese un mensaje" data-msg="Ingrese un mensaje" data-require="Campo requerido"></textarea>
					</div>
				</div>
				<div class="col-lg-12 animate">
					<div class="form-group">
						<button class="button-contact">ENVIAR</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection