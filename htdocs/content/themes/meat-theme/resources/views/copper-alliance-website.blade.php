@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "copper alliance websites", 'backgroundImage' => $banner,'setTitle' => 'set-title-caws'])
	<div class="body-websites">
		<div class="body-nosotros set-color ica">
			@include('components/website-ica-en-el-mundo', ['subtitle' => $subtitle, 'content' => $content])
		</div>
	</div>
</div>
@endsection