@extends('layout/default')
@section('content')

<div class="content-slider">
	@include('components/home-slider')
</div>
<div class="content-slider-separator">
	<div class="box-color box-color-1"></div>
	<div class="box-color box-color-2"></div>
	<div class="box-color box-color-3"></div> 
	<div class="box-color box-color-4"></div>
</div>

@if(!empty($section_us))
	<div class="content-card animate">
		<div class="card">
			<div class="image" style="background-image: url('{{ $section_us['image_us'] }}');">
				<img alt="{{ $section_us['title_us'] }}" src="{{ $section_us['image_us'] }}" alt="">
			</div>
			<div class="body-text">
				<h2 class="title">{{ $section_us['title_us'] }}</h2>
				<hr class="line">
				{!! $section_us['description_us'] !!}
				@if($section_us['link_us'])
					<a href="{{ $section_us['link_us'] }}" class="super-button set-position"><span>conóce más sobre nosotros</span></a>
				@endif
			</div>
		</div>
	</div>
@endif
@include('components/home-video')
@if($trens=='quit_this')
	<div class="social-benefits">
		<div class="body-social-benefits">
			<h2 class="title">{{ $section_benefits['title_benefits'] }}</h2>
			<hr class="line">
			<span class="text">{!!  strip_tags($section_benefits['description_benefits'], "<p>") !!}</span>
		</div>

		<div class="content-target-benefits">
			@foreach($trens as $trens_post)
			<div class="target animate">
				<div class="icon">
					<img src="{{ $trens_post['image']  }}" alt="{{ $trens_post['title'] }}">
				</div>
				<h3 data-equalize="beneficio-title" class="title">{{ $trens_post['title'] }}</h3>
				<div class="text" data-equalize="beneficio-text">
					{!!  $trens_post['description'] !!}
				</div>
				<a href="{{ $trens_post['link']  }}" class="super-button"><span>VER MÁS</span></a>
			</div>
			@endforeach
		</div>
	</div>
@endif
<div class="applications">
	<h2>{{ $section_aplications['title_aplications'] }}</h2>
	<hr class="line">
	{!!  $section_aplications['description_aplications'] !!}
	<div class="applications-content-circle">
		@include('components/home-applications-circle')
	</div>
</div>
@if(!empty($publications))
<div class="last-news">
	<h2>ÚLTIMAS NOTICIAS</h2>
	<hr class="line">
	<div class="content-last-news">
		@foreach($publications as $publication)
			<div class="target-last-news animate">
				<div class="target-last-news-image">
					<img src="{{ $publication['image'] }}" alt="{{ $publication['title'] }}">
				</div>
				<div class="button-float">
					<p>{{ $publication['type_cat'] }}</p>
				</div>
				<div class="target-last-news-cuerpo" data-equalize="last-news-title">
					<h3>{{ $publication['title'] }}</h3>
				</div>
				<div class="target-last-news-time">
					<i class="fa fa-clock-o" aria-hidden="true"><span>{{ $publication['date'] }}</span></i>
				</div>
				<div class="target-last-news-text" data-equalize="last-news-text">
					{!! $publication['description'] !!}
				</div>
				<a href="{{ $publication['link'] }}" class="button super-button"><span>VER MÁS</span></a>
			</div>
		@endforeach
	</div>
	<br>
	<br>
	<a href="/publicaciones" class="button super-button">IR A NOTICIAS Y PUBLICACIONES</a>
	<div class="newsletter animate">
		<div class="content-text">
			<p>SUSCRIBETE A NUESTRO NEWSLETTER</p>
		</div>
		<div class="content-search">
			<input class="search-email" type="" name="" placeholder="Ingresa tu e-mail">
			<button class="search-button"><a class="off-select" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i></a></button>
		</div>
	</div>	
</div>
@endif
@endsection