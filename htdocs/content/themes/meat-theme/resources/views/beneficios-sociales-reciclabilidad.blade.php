@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', [ 'textBanner' => '', 'breadcrumb' => '', 'mainTitle' => "beneficios sociales", 'backgroundImage' => $banner,'setTitle' => ''])
	<div class="el-cobre-interior-body">
		@if(!empty($page_children))
			<div class="el-cobre-interior-body">
				<div class="nav nav-tabs el-cobre-interior-body-filter">
					@foreach($page_children as $benefits)
						<a href="{{ Loop::link($benefits->ID) }}"><button class="button-tabs {{ $pagename == $benefits->post_name ? 'active' : '' }}">{{ $benefits->post_title }}</button></a>
					@endforeach
				</div>
			</div>
		@endif
		<div class="tab-content">
			<div id="menu1" class="default-height-tabs">
				<div class="el-cobre-interior-content">
					<h1 class="animate">{{ $title }}</h1>
					<hr class="line animate">
				</div>
				<div class="el-cobre-body set-el-cobre-body-tree content-list-text">
					{!!  apply_filters('the_content', $content); !!}
				</div>


				@if($features)
					@foreach($features as $value_features)

						@if($loop->iteration % 3 == 1)
							<div class="progress-circle ca-top animate">
								@endif
								<div class="circle circle-md circle-copper">
									<svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
										<circle class="back" cx="50" cy="50" r="40" stroke="#E5E7E7" stroke-width="4" fill="transparent" />
										<circle class="front" cx="50" cy="50" r="40" stroke="#8E6250" stroke-width="4" fill="transparent" />
									</svg>
									<div class="precentage_wrapper">
										<span class="valor small-percentage" data-percentage="{{ $value_features['porcentaje'] }}">{{ $value_features['porcentaje'] }}</span>%
									</div>
									<p>{{ $value_features['titulo'] }}</p>
								</div>
								@if($loop->iteration % 3 == 0)
							</div>
						@endif
					@endforeach
				@endif
			</div>
			@include('components/block-shares')
		</div>
	</div>
</div>
@endsection