@extends('layout/default') 
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => $short_text, 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => 'Publicaciones', 'backgroundImage' => $image_banner,'setTitle' => ''])
	<div class="documents-body">
		@if(!empty($categories))
		<div class="documents-menu">
			@foreach($categories as $category)
				<a href="{{ get_term_link($category) }}" class="documents-button documents-button-1 {{ $term_slug ==  $category->slug ? 'active' : ''}}">{{ $category->name }}</a>
			@endforeach
		</div>
		@endif
		<div class="documents-head animate">
			<div class="documents-head-image">
				<img src="{{ $image  }}" alt="{{ $term_name }}">
			</div>
			<div class="documents-head-body">
				<h2 class="title">{{ $term_name }}</h2>
				<hr class="line">
				<p class="text-826252">{!!   $description !!}</p>
			</div>
		</div>
		@include('components/filter-pages')
		<div class="documents-target new-filter-product">
			@if(!empty($publications))
				@foreach($publications as $publication)
					@include('components/documento-detalle-target', [
						'title' 		=> $publication['title'],
						'description' 	=> $publication['description'],
						'image'			=> $publication['image'],
						'type_cat'		=> $publication['type_cat'],
						'date'			=> $publication['date'],
						'link'			=> $publication['link']
						])
				@endforeach
			@endif

			<div class="content-paginator animate">
				@php
				/*
				$pp=7;
				$total = count($publications);
				$current_page =1;
				$page_amount = $total/7;
				$limiter = 8;
				*/
				$pp=4;
				$total = count($count_publication);
				$current_page =1;
				$page_amount = $total/2;
				$limiter = 8;

				$sides = round(($limiter/2), 0, PHP_ROUND_HALF_DOWN);
				@endphp
				<div class="nav-links pagitantionProduct">
					<a class="prev page-numbers" data-pp="{{ $pp }}" data-accion="anterior" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
					<div style="display: none;" class="page_actual">1</div>
					<div style="display: none;" class="page_total">{{ $page_amount  }}</div>

					@php

					if ($current_page > ($limiter/2)){
						echo  '<a href="javascript:void(0)" id="pv1" data-page="1">1 </a><a> ...</a> ';
					}
					for ( $counter = 1; $counter <=  $page_amount; $counter++) {

						if($counter >= ($current_page)){
							if(($counter <($current_page+$sides))||($counter >($page_amount-$sides))){
								$num_page = $counter+1;

								echo '<a class=" page-numbers set-border" id="pv'.$num_page.'" href="javascript:void(0)" data-pp="'.$pp.'" data-page="'.$num_page.'">'.$num_page.'</a>';

							}elseif($counter ==($current_page+$sides)){
								$num_page = $counter+1;
								echo '<a class=" page-numbers set-border" id="pv'.$num_page.'" href="javascript:void(0)" data-pp="'.$pp.'" data-page="'.$num_page.'">';

								if(($page_amount-$current_page)==$limiter-1){
									echo $counter+1;
								}else {
									echo "...";
								}
								echo "</a> ";
							}
						}
					}
					@endphp

					<a class="next page-numbers" data-pp="{{ $pp }}" data-accion="siguiente" href="#"> <i class="fa fa-angle-right"></i></a>

				</div>
				<!-- @ include('components/paginator') -->
			</div>
		</div>
	</div>
</div>
@endsection