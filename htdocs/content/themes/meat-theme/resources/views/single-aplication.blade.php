@extends('layout/default')
@section('content')
    <div class="el-cobre">
        @include('components/banner-default-aplicaciones' , ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => $title_single, 'backgroundImage' =>  $banner,'setTitle' => ''])
        @include('components/aplicaciones-sidebar-mobile')
        <div class="container body-aplicaciones">
            <div class="col-lg-4 col-sm-0 col-sm-5 section-relative hidden-sm">
                @include('components/aplicaciones-sidebar')
            </div>
            <div class="col-lg-8 col-sm-12  section-relative">
                @include('components/aplicaciones-body', ['title' => $title_single])
            </div>
        </div>
    </div>
@endsection