@extends('layout/default')
@section('content')
<div class="el-cobre">
	@include('components/banner-default-pages', ['textBanner' => '', 'breadcrumb' => 'breadcrumb-out', 'mainTitle' => "Nosotros", 'backgroundImage' => $banner,'setTitle' => ''])
	@include('components/nosotros-sidebar-mobile')
	<section class="scroll-nav__section">
	<span id="scrollNav-1" class="section-hidden"></span>
		<div class="container body-aplicaciones">
			<div class="col-lg-4 section-relative">
				@include('components/nosotros-sidebar')
			</div>
			<div class="col-lg-8 section-relative">
				<div class="body-text-aplicaciones set-box-text">
					<!--<div class="text-bajada">

					</div>-->
					<br><br>
					{!! $description['content'] !!}
					@if(!empty($description['image_content']))
						<div class="image-text animate">
							<img src="{{ $description['image_content']  }}" alt="Nosotros">
						</div>
					@endif
				</div>
			</div>
		</div>
	</section>
	<section class="scroll-nav__section">
		<span id="scrollNav-2" class="section-hidden"></span>
		<div class="body-nosotros vision animate">
			@include('components/nosotros-vision', ['title' => $vision['title'], 'content' => $vision['content']])
		</div>
	</section>

	<section class="scroll-nav__section">
		<span id="scrollNav-3" class="section-hidden"></span>
		<div class="body-nosotros set-color mision animate">
			@include('components/nosotros-mision', ['title' => $mision['title'], 'content' => $mision['content'], 'features' => $mision['features']])
		</div>
	</section>

	<section class="scroll-nav__section">
		<span id="scrollNav-4" class="section-hidden"></span>
		<div class="body-nosotros propuesta animate">
			@include('components/nosotros-propuesta-de-valor',['title' => $propuesta['title'], 'content' => $propuesta['content'], 'file' => $propuesta['file']])
		</div>
	</section>

	<section class="scroll-nav__section">
		<span id="scrollNav-5" class="section-hidden"></span>
		<div class="body-nosotros set-color ica animate">
			@include('components/nosotros-ica-en-el-mundo', ['title' => $offices['title'], 'content' => $offices['content'], 'offices' => $offices['Offices']])
		</div>
	</section>
</div>
@endsection