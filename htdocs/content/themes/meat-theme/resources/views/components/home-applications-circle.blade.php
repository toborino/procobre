@if(!empty($aplications))
	@foreach($aplications as $aplication)
		<a class="off-select" href="{{ $aplication['link'] }}">
			<div class="circle animate" style="background-image: url('{{ $aplication['image'] }}'); ">
				<div class="filter-color"></div>
				<h3>{{ $aplication['title'] }}</h3>
			</div>
		</a>
	@endforeach
@endif