<div class="nav-links">
	<a class="prev page-numbers set-border" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
	<a class="page-numbers set-border current" href="#">10</a>
	<a class="page-numbers set-border" href="#">11</a>
	<span class="page-numbers set-border current">...</span>
	<a class="page-numbers set-border" href="#">25</a>
	<a class="page-numbers set-border" href="#">26</a>
	<a class="next page-numbers" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>