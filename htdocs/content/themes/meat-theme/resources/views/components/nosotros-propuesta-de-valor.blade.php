<div class="container body-aplicaciones">
	<div class="col-lg-8 col-lg-offset-4 section-relative">
		<div class="body-text-aplicaciones set-box-text">
			<h2 class="body-nosotros-title">{{ $title }}</h2>
			<hr class="line">
			{!! $content !!}
			@if(!empty($file))
				<a target="_blank" href="{{ $file }}" class="button super-button"><span>DESCARGAR PROPUESTA</span></a>
			@endif
		</div>
	</div>
</div>