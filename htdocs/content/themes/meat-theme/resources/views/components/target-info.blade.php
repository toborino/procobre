{{-- Formato para ver videos --}}

@if(!empty($biblioteca))
    @foreach($biblioteca as $post_biblioteca)
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="block-info">
                <div class="body-text">
                    <h3>{{ $post_biblioteca['category'] }}</h3>
                    <p data-ecualize="title_biblio">{{ $post_biblioteca['title'] }}</p>
                    @if($post_biblioteca['type'] == 'Video')
                        <button data-youtube="{{ $post_biblioteca['video'] }}">VER VIDEO</button>
                    @elseif($post_biblioteca['type'] == 'Link')
                        <a href="{{ $post_biblioteca['link'] }}" target="_blank">LEER MÁS</a>
                    @elseif($post_biblioteca['type'] == 'Descarga')
                        <a class="button" target="_blank" href="{{ $post_biblioteca['file'] }}">Descargar</a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@endif
