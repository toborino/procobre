<div class="contennt-small-menu-top" >
	<div class="small-menu-top">
		<div class="content-select">
			<i class="fa fa-angle-down" aria-hidden="true"></i>
			<select class="form-control Languages">
					<option>ES</option>
					<option>PT</option>
			</select>
	  	</div>
		<div class="search button-search">
			<i class="fa fa-search" aria-hidden="true"></i>
		</div>
	  	<div class="rrss">
	  		<a href="https://twitter.com/thinkcopper"><i class="fa fa-twitter" aria-hidden="true"></i>
	  		<p>@InfoCobre</p></a>
	  	</div>

	  	<div class="members">
	  		<div class="open-login">
			  	<img src="{{ get_bloginfo('template_url') }}/dist/images/user.svg" alt="">
			  	<p>MIEMBROS ICA LA</p>
		  	</div>
		  	<div class="content-login">
				<div class="signin">
					<div class="login">
						<form class="loginform form-signin"  id="loginform" action="<?php echo get_option('home'); ?>/cms/wp-login.php" method="post">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="ca-email">USUARIO O EMAIL :</label>
								<input type="text" class="form-control input-ca"name="log" id="user_login" aria-describedby="emailHelp" placeholder="Ingrese su usuario o email">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label class="set-margin-label" for="ca-password">CONTRASEÑA :</label>
								<input type="password" class="form-control input-ca" id="user_pass" name="pwd" aria-describedby="emailHelp" placeholder="Ingrese su contraseña">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<input type="submit" name="wp-submit" id="wp-submit" class="button-contact" value="INGRESAR" tabindex="100" />
									<input type="hidden" name="redirect_to" value="<?php echo get_option('home'); ?>/miembros-articulos/" />
								</div>
							</div>
						</form>

						<div class="col-lg-12 content-other-button">
							<div class="form-group">
								<a href="#">Olvidaste tu contraseña</a>
								<a href="#">Solicitar acceso</a>
							</div>
						</div>
						<i class="fa fa-times close-signin" aria-hidden="true"></i>
					</div>
				</div>
		  	</div>
		</div>
	  	<a href="/copper-alliance-website">
			<div class="websites">
	  			<p>COPPER ALLIANCE WEBSITES</p>
	  		</div>
		</a>
	</div>
</div>