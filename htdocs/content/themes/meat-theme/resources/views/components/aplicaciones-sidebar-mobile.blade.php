<div class="sidebar-mobile">
	<div class="sd-head">
		<a class="accordion-toggle">
		<span>Estás en:</span><br> {{ $title_single }}</a>
	</div>
	<div class="sd-body">
		<ul class="sd-body-menu">

			@if(!empty($post_menu))
				@foreach($post_menu as $menu)
					<li>
						<a href="{{ $menu['link'] }}">{{ $menu['title'] }}</a>
						<i class="fa fa-arrow-right {{ $menu['slug'] == $current_slug ? 'active' : '' }}" aria-hidden="true"></i>
					</li>
				@endforeach
			@endif
		</ul>
	</div>
</div>