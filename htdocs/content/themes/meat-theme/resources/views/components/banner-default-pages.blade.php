<div class="banner-default-pages js-bgimage-loaded">
	@if( isset($backgroundImage) )
    	<div class="background-image-bg imagesloaded u-cover u-absolute bgZoom" style="background-image: url('{{ $backgroundImage }}');"></div>
  	@endif

	<div class="header-content">
		<div class="breadcrumb {{ $breadcrumb }}">
			<p class="set-margin">Inicio </p>
			<span>></span>
			<p>Beneficions Sociales</p>
			<span>></span>
			<p>{{ get_the_title() }}</p>
		</div>
		<h1 class="{{ $setTitle }}">{{ $mainTitle }}</h1>
		<p>{{ $textBanner }}</p>
	</div>
</div>