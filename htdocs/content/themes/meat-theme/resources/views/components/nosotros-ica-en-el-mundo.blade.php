<div class="container body-aplicaciones">
  <div class="col-lg-8 col-lg-offset-4 section-relative">
    <div class="body-text-aplicaciones set-box-text">
      <h2 class="body-nosotros-title">{{ $title }}</h2>
      <hr class="line">
      {!! $content !!}


      <div class="filter-ica">
        @foreach($offices as $office)
          <div class="tab-buttons tab-buttons-{{ $loop->iteration }} {{ $loop->iteration == 1 ? 'active' : '' }}">{{ $office['continent'] }}</div>
        @endforeach
      </div>
      <div class="filter-ica-mobile">
        <div class="ica-head">
          <a class="accordion-toggle"><span>Seleccione</span><br>una localidad:</a>
        </div>
        <div class="ica-cuerpo">
          <ul>
            @foreach($offices as $office)
              <div class="tab-buttons tab-buttons-{{ $loop->iteration }} {{ $loop->iteration == 1 ? 'active' : '' }}">{{ $office['continent'] }}</div>
              <li class="tab-buttons-{{ $loop->iteration }}">
                <i class="fa fa-1 fa-arrow-down {{ $loop->iteration == 1 ? 'active' : '' }}" aria-hidden="true"></i>
                {{ $office['continent'] }}
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>

  @foreach($offices as $office)
      <div class="col-lg-8 col-lg-offset-4 section-relative ica-body ica-body-{{ $loop->iteration }}" style="{{ $loop->iteration == 1 ? 'display: block' : 'display: none' }}">
        @foreach($office['liste'] as $country)
            @if($loop->iteration == 1)
                <div class="row">
                    <div itemscope itemtype="http://data-vocabulary.org/LocalBusiness">
                        <div xmlns:v="http://rdf.data-vocabulary.org/#">
                            <div class="body-text-ica set-box-text">
                                @if($office['hide_title'] != 1)
                                    @if(!empty($office['continent']))
                                        <h2 class="title-ica">OFICINA PRINCIPAL DE {{ $office['continent'] }}</h2>
                                    @endif
                                @else
                                    <h2 class="title-ica"></h2>
                                @endif
                                @if(!empty($office['logo_continente']['url']))
                                        <div class="logo-country">
                                            <img src="{{ $office['logo_continente']['url'] }}" alt="">
                                        </div>
                                @endif

                                    <div class="body-sub-title">
                                        <h3 itemprop="addressLocality" property="v:title" class="title-local">{{ $country['name_office'] }}</h3>
                                    </div>

                                <div class="col-lg-6 set-bottom-section">
                                    <div class="content-list-text">
                                      <ul>
                                         @if(!empty($country['web_site']))

                                                  <li class="site before-icon">
                                                    <p class="text"><a itemprop="url" rel="v:url" href="{{ $country['web_site'] }}">{{ $country['web_site'] }}</a></p>
                                                    </li>

                                         @endif
                                         @if(!empty($country['addres']))
                                            <li class="marker before-icon">
                                            <p class="text">{{ $country['addres'] }}</p>
                                            </li>
                                         @endif
                                      </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 set-bottom-section">
                                    <div class="content-list-text">
                                      <ul>
                                          @if(!empty($country['telephone']))
                                            <li class="phone before-icon">
                                            <p class="text">{{ $country['telephone'] }}</p>
                                            </li>
                                          @endif
                                          @if(!empty($country['encargado']))
                                             <li class="user before-icon">
                                             <p class="text">{{ $country['encargado'] }}</p>
                                             </li>
                                          @endif
                                      </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($loop->iteration > 1)
                    <h2 class="title-ica">OFICINAS LOCALES</h2>
                @endif
            @else
                <div  class="row">
                    <div itemscope itemtype="http://data-vocabulary.org/LocalBusiness">
                        <div xmlns:v="http://rdf.data-vocabulary.org/#">
                            <div  class="body-text-ica set-box-text">
                                <div class="body-sub-title">

                                        <h3 itemprop="addressLocality" property="v:title" class="title-local">{{ $country['name_office'] }}</h3>


                                    <!--<p class="title-bajada">Procobre-Brazil</p>-->
                                </div>
                                 <div class="col-lg-6">
                                    <div class="content-list-text">
                                          <ul>
                                             @if(!empty($country['web_site']))
                                                <li class="site before-icon">
                                                   <p class="text"><a  itemprop="url" rel="v:url" href="{{ $country['web_site'] }}">{{ $country['web_site'] }}</a></p>
                                                </li>
                                             @endif
                                             @if(!empty($country['addres']))
                                                <li class="marker before-icon">
                                                    <p itemprop="address" class="text">{{ $country['addres'] }}</p>
                                                </li>
                                             @endif
                                          </ul>
                                    </div>
                                 </div>


                                  <div class="col-lg-6">
                                    <div class="content-list-text">
                                      <ul>
                                        @if(!empty($country['telephone']))
                                            <li class="phone before-icon">
                                                <p itemprop="telephone" class="text">{{ $country['telephone'] }}</p>
                                            </li>
                                        @endif
                                        @if(!empty($country['encargado']))
                                            <li class="user before-icon">
                                                <p itemprop="name" class="text">{{ $country['encargado'] }}</p>
                                            </li>
                                        @endif
                                      </ul>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        @endforeach
      </div>
  @endforeach

</div>