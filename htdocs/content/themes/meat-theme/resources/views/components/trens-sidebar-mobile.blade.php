<div class="sidebar-mobile">
    <div class="sd-head">
        <a class="accordion-toggle">
            <span>Estás en:</span><br> {{ $title_single }}</a>
    </div>
    <div class="sd-body">
        <ul class="sd-body-menu">

            @if(!empty($aplications))
                @foreach($aplications as $value_aplication)
                    <li>
                        <a href="{{ $value_aplication['link'] }}">{{ $value_aplication['title'] }}</a>
                        <i class="fa fa-arrow-right {{ $value_aplication['slug'] == $current_slug ? 'active' : '' }}" aria-hidden="true"></i>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>