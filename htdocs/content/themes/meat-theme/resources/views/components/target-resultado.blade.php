<div class="target-last-news animate">
	<div class="target-last-news-image">
		<img src="{{ $image }}" alt="">
  </div>
  <div class="target-last-news-cuerpo" data-equalize="title" >
  	<h3>{{ $title }}</h3>
  </div>
	<a href="{{ $link }}" class="button super-button"><span>VER MÁS</span></a>
</div>