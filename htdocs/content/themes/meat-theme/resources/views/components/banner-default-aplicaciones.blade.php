<div class="banner-default-pages js-bgimage-loaded">

	@if( isset($backgroundImage) )
    <div class="background-image-bg imagesloaded u-cover u-absolute bgZoom" style="background-image: url(' {{ $backgroundImage }}');"></div>
  @endif

  <div class="header-content header-content-aplicaciones"> 
  	<div class="breadcrumb {{ $breadcrumb }}">
  		<p class="set-margin">Inicio</p>
  		<span>></span>
  		<p>El Cobre</p>
  		<span>></span>
  		<p>{{ Loop::title() }}</p>
  	</div>
    <h2 class="{{ $setTitle }}">{{ $mainTitle }}</h2>
    <p>{{ $textBanner }}</p>
  </div>
</div>