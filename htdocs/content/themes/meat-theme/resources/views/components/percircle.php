<div class="container">
	<div class="row" style="width: 100%; height: 400px; background-color: red;">
		<div class="circle">
	  	<svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
	    <circle class="back" cx="50" cy="50" r="40" stroke="#cddc39" stroke-width="4" fill="transparent" />
	    <circle class="front" cx="50" cy="50" r="40" stroke="#9e9d24" stroke-width="4" fill="transparent" />
  		</svg>
	  <div class="precentage_wrapper">
	    <input type="text" max="100" min="0" name="percentage" value="87">
		</div>
</div>