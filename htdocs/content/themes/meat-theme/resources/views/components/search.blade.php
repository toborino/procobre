<div id="content-search">
	<div class="container">
		<div class="row">
			<form action="{{ home_url() }}" method="get">
				<input type="text" name="s" id="search" placeholder="Escribe aquí para buscar">
				<button class=""><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
			</form>
		</div>
	</div>
</div>