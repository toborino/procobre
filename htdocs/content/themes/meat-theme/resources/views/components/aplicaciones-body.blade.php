<h1>{{ $title }}</h1> 
<hr class="line"></hr>
@if(!empty($content_image))
<div class="video animate" style="background-image: url('{{ $content_image }}');">
	<img src="{{ $content_image }}" alt="{{ $title }}">
</div>
@endif
<br>
<div class="body-text-aplicaciones animate">
	{!! $content_single !!}
</div>