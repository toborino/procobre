<div class="contennt-small-menu-top contennt-small-menu-top-user top-menu-on">
	<div class="small-menu-top">
		<div class="content-select">
			<i class="fa fa-angle-down" aria-hidden="true"></i>
			<select class="form-control Languages">
				<option>ES</option>
				<option>PT</option>
			</select>
		</div>
		<div class="search button-search">
			<i class="fa fa-search" aria-hidden="true"></i>
		</div>
		<div class="rrss">
			<a href="{{ wp_logout_url( home_url() ) }}">Cerrar sesión</a>
		</div>
		<a href="{{ home_url() }}/miembros-articulos/">
		<div class="members">
			<img src="{{ get_bloginfo('template_url') }}/dist/images/user.svg" alt="">
			<p>Bienvenido, {{ $display_name }}</p>
		</div>
		</a>
		<div class="button-volver">
			<i class="fa fa-arrow-left" aria-hidden="true"></i>
			<p class="back-copper"><a href="{{ home_url() }}"> VOLVER A COPPER ALLIANCE</a></p>
		</div>
	</div>
</div>