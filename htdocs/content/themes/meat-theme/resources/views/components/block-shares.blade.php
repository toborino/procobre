<div class="block-shared animate">
	<p>COMPARTIR</p>
	<a href="{{ Loop::link() }}" rel="nofollow" class="share facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
	<a href="{{ Loop::link() }}" rel="nofollow" data-text="{{ Loop::title() }}" class="share twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
</div>