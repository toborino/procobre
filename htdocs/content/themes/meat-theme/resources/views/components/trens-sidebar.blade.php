<div class="post__article sidebar animate">
    <ul>
        @if(!empty($post_menu))
            @foreach($post_menu as $menu)
                <a href="{{ $menu['link'] }}"><li class="{{ $menu['slug'] == $current_slug ? 'current-active' : '' }}">{{ $menu['title'] }}</li></a>
            @endforeach
        @endif
    </ul>
</div>

