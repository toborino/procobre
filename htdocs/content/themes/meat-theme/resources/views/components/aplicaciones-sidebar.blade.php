
<div class="post__article sidebar animate">
	<ul>
		@if(!empty($aplications))
			@foreach($aplications as $value_aplication)
				<a href="{{ $value_aplication['link'] }}"><li class="{{ $value_aplication['slug'] == $current_slug ? 'current-active' : '' }}">{{ $value_aplication['title'] }}</li></a>
			@endforeach
		@endif
	</ul>
</div>

