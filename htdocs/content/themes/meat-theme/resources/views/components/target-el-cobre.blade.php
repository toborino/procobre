<div class="target-el-cobre animate">
	<div class="target-el-cobre-image" style="background-image: url('{{ $image  }}');">
		<img src="{{ $image  }}" alt="{{ $title }}">
	</div>
	<div class="target-el-cobre-text">
		<h2>{{ $title }}</h2>
		{!!  $short_description !!}
		<a href="{{ $link  }}" class="button super-button"><span>VER MÁS</span></a>
	</div>
</div>