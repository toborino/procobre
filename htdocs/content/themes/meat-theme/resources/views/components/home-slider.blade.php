@php
$count = 1;
@endphp

<ul class="slider-home">
		@if(!empty($slider))
			@foreach($slider as $slide)
				<li class="back-color">
					<div class="slider-home-image imagesloaded" style="background-image: url('{{ $slide['image'] }}');">
					</div>
					<div class="body-slider">
						<div class="row flex" style="display: none;">
							<hr class="line">
							<span>{{ $count }} / <span class="count"></span></span>
						</div>
						<div class="row">
							<h2 class="title title-slider fadeInUp">{{ $slide['title'] }} </h2>
							<p class="paragrap fadeInUp">{!!  $slide['description'] !!}</p>
							@if($slide['link'])
								<a href="{{ $slide['link']  }}" class="button fadeInUp">VER MÁS</a>
							@endif
						</div>
					</div>
				</li>
		@php
$count++;
@endphp
		@endforeach
	@endif
</ul>
<div class="content-scroll">
	<h4>SCROLL</h4>
	<i class="fa fa-arrow-down" aria-hidden="true"></i>
</div>