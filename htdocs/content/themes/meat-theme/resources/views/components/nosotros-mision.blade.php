<div class="container body-aplicaciones">
	<div class="col-lg-8 col-lg-offset-4 section-relative">
		<div class="body-text-aplicaciones set-box-text">
			<h2 class="body-nosotros-title">{{ $title }}</h2>
			<hr class="line">
			{!! $content !!}
		</div>
		@if(!empty($features))
			@foreach($features as $list)
				<div class="col-lg-6">
					<h2 class="body-nosotros-title-in">{{ $list['name_features'] }}</h2>
					<div class="content-list-text">
						<ul>
							@foreach($list['liste'] as $value)
								<li>
									<p class="text">{{ $value['listado'] }}</p>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endforeach
		@endif

	</div>
</div>