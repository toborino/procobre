<div class="sidebar-mobile nosotros-sidebar-mobile">
	<div class="sd-head"> 
		<a class="accordion-toggle">
		<span>Estás en:</span><br> Nosotros</a> 
	</div>
	<div class="sd-body">
		<ul class="sd-body-menu">
			<a href="#scrollNav-1"><li class="selector-1">
				DESCRIPCION
				<i class="fa fa-1 fa-arrow-down active" aria-hidden="true"></i>
			</li></a>
			<a href="#scrollNav-2"><li class="selector-2">
				VISIÓN
				<i class="fa fa-2 fa-arrow-down" aria-hidden="true"></i>
			</li></a>
			<a href="#scrollNav-3"><li class="selector-3">
				MISIÓN
				<i class="fa fa-3 fa-arrow-down" aria-hidden="true"></i>
			</li></a>
			<a href="#scrollNav-4"><li class="selector-4">
				PROPUESTA DE VALOR
				<i class="fa fa-4 fa-arrow-down" aria-hidden="true"></i>
			</li></a>
			<a href="#scrollNav-5"><li class="selector-5">
				ICA EN EL MUNDO
				<i class="fa fa-5 fa-arrow-down" aria-hidden="true"></i>
			</li></a>
		</ul>
	</div>
</div>