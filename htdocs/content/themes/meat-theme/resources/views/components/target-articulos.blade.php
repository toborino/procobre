<div class="target-last-news animate">
	<div class="target-last-news-image">
		<img src="{{ $image }}" alt="{{ $title }}">
  </div>
  <div class="button-float">
  	<p>{{ $type_cat }}</p>
  </div>
  <div class="target-last-news-cuerpo">
  	<h3>{{ $title }}</h3>
  </div>
  <div class="target-last-news-time">
  	<i class="fa fa-clock-o" aria-hidden="true"><span>{{ $date }}</span></i>
  </div>
  <!--<div class="target-last-news-text">
		{!!  $description  !!}
	</div>-->
	<a href="{{ $link }}" class="button super-button"><span>VER MÁS</span></a>
</div>