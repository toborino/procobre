<div class="documents-filter animate">
	<div class="current-category" data-cat="{{ $term_slug }}"></div>
	<p class="title">FILTRAR POR:</p>
	<a href="{{ $link_cat }}" ><button class="button-filter">TODO</button></a>
	<p class="title">CATEGORIA: </p>
	<div class="content-select">
		<i class="fa fa-angle-down" aria-hidden="true"></i>
		@if(!empty($cate_type))
			<select name="filter_cat" class="filterCategory">
				<option value=""> Seleccione...</option>
				@foreach($cate_type as $type)
					<option value="{{ $type->slug }}">{{ $type->name }}</option>
				@endforeach
			</select>
		@endif
	</div>
	<p class="title">FECHA:</p>
	<div class="content-select">
		<i class="fa fa-angle-down" aria-hidden="true"></i>
		<input class="datepicker" type="" name="" placeholder="SELECCIONE UNA FECHA">
	</div>
	<!--<p class="title">PAIS:</p>
	<div class="content-select">
		<i class="fa fa-angle-down" aria-hidden="true"></i>
		<select class="title">
			<option>BRASIL</option>
			<option>AFRICA</option>
			<option>OTRO</option>
		</select>
	</div>-->
</div>