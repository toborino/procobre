<div class="el-cobre-interior-content">
	<h1 class="animate">{{ $title }}</h1>
	<hr class="line animate">
</div>
@if(!empty($image_video))
<div class="content-video-interior animate">
	<img src="{{ $image_video  }}" alt="{{ $title }}">
	@include('components/button', ['video_id' => $video_id])
</div>
@endif
<div class="el-cobre-body set-el-cobre-body animate content-list-text animate animate-in">

	{!!  apply_filters('the_content', $content); !!}
</div>
@include('components/block-shares')