@if(!empty($video_id))
	<div class="btn-play video-on">
		<button class="button-generic" data-youtube="{{ $video_id }}">
		<i class="fa fa-play" aria-hidden="true"></i></button>
	</div>
@endif