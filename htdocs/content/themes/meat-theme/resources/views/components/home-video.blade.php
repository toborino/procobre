
@if($section_cobre)
<div class="content-video" style="background-image: url('{{ $section_cobre['image_video']  }}')">
	<img src="{{ $section_cobre['image_video']  }}" alt="{{ $section_cobre['title_video']  }}">
	<div class="body-video">
		<h2 class="title">{{ $section_cobre['title_video']  }}</h2>
		<hr class="line">
		<p class="text">{!! $section_cobre['description_video'] !!}</p>
		@include('components/button', ['video_id' => $section_cobre['id_video']])
		@if($section_cobre['link_video'])
		<div> 
			<a href="{{ $section_cobre['link_video'] }}" class="button">VER MÁS</a>
		</div>
		@endif
	</div>
	<div class="trama">
	</div>
</div>
@endif