<div id="blur"></div>
<div class="menu-mobile">
	<div class="content-logo">
		<a href="/"><img src="{{ get_bloginfo('template_url') }}/dist/images/logotipo-copper-alliance.svg" alt=""/></a>
	</div>
</div>
<div id="menu-mobile">
	<div class="search-mobile"> 
		<form method="get">
			<input type="text" name="search" id="search" placeholder="Escribe aquí para buscar">
			<button class="close-search"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
		</form>
	</div> 
	<div class="content-logo">
		<img src="{{ get_bloginfo('template_url') }}/dist/images/logo-copper-dark.svg" alt=""/>
		<div class="button-search button-search-mobile"> 
				<i class="fa fa-search" aria-hidden="true"></i>
		</div>	
	</div>
	<div class="content-menu">
		<ul>
			{{ wp_nav_menu_top() }}
			<li><a href="/contacto">CONTACTO</a></li>
		</ul>
		<hr class="separator" />
		<a href="/copper-alliance-website"><div class="content-button">
			<p class="button-caws">COPPER ALLIANCE WEBSITES</p> 
		</div></a>
		<div class="content-button content-button-caws">
			<img src="{{ get_bloginfo('template_url') }}/dist/images/user.svg" alt="">
			<p class="button-miembros">MIEMBROS ICA LA</p> 
		</div>
		<hr class="separator separator-top"/>
	</div>
</div>

<div id="content-button">
	<button class="hamburger hamburger--collapse" type="button">
	 	<span class="hamburger-box"><span class="hamburger-inner"></span></span>
	</button>
</div>

<div class="login-mobile"> 
		<div class="signin"> 
			<div class="login">
				<form class="form-signin">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="ca-email">EMAIL :</label> 
							<input type="email" class="form-control input-ca" id="ca-email" aria-describedby="emailHelp" placeholder="Ingrese su email">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="set-margin-label" for="ca-password">CONTRASEÑA :</label> 
							<input type="password" class="form-control input-ca" id="ca-password" aria-describedby="emailHelp" placeholder="Ingrese su contraseña">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<button class="button-contact">ENVIAR</button>
						</div>
					</div>
				</form>
				<div class="col-lg-12 content-other-button">
				<div class="form-group">
					<a href="#">Olvidaste tu contraseña</a>
					<a href="#">Solicitar acceso</a>
				</div>
			</div> 
			<i class="fa fa-times close-signin close-signin-mobile" aria-hidden="true"></i>
		</div> 
	</div>
</div>