<?php

/**
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

flush_rewrite_rules();

Route::get('front', [ 'uses' => 'HomeController@index' ]);
Route::get('page', ['el-cobre', 'uses' => 'CopperController@copper']);


Route::get('page', ['aleaciones', 'uses' => 'CopperController@pagesCopper']);
Route::get('page', ['historia-del-cobre', 'uses' => 'CopperController@pagesCopper']);
Route::get('page', ['propiedades', 'uses' => 'CopperController@pagesCopper']);
Route::get('page', ['innovacion-y-reciclaje-en-mineria-del-cobre', 'uses' => 'CopperController@pagesCopper']);
Route::get('page', ['disponibilidad-largo-plazo', 'uses' => 'CopperController@pagesCopper']);
Route::get('page', ['acerca-del-cobre', 'uses' => 'CopperController@pagesCopper']);



Route::get('page', ['beneficios-sociales', 'uses' => 'BenefitsController@benefits']);

Route::get('page', ['esencialidad', 'uses' => 'BenefitsController@benefitsEsencialidad']);
Route::get('page', ['reciclabilidad', 'uses' => 'BenefitsController@benefitsReciclabilidad']);
Route::get('page', ['sustentabilidad', 'uses' => 'BenefitsController@benefitsSustentabilidad']);



Route::get('template', ['trens-innovations', 'uses' => 'TrensInnovationsController@trens']);

Route::get('singular', ['trens-innovations', 'uses' => 'TrensInnovationsController@single']);

Route::get('singular', ['aplications', 'uses' => 'AplicationsController@show']);

Route::get('page', ['beneficio-del-cobre', function() {
    return view('aplicaciones');
}]);

Route::get('page', ['nosotros', 'uses' => 'UsController@us']);

Route::get('page', ['publicaciones', 'uses' => 'PublicationsController@publications']);


Route::get('tax', ['Categorias',  'uses' => 'PublicationsController@CategoryPublications']);

Route::get('page', ['aplicacion', function() {
    return view('aplicaciones');
}]);

Route::get('page', ['publicaciones', 'uses' => 'PublicationsController@publications']);

Route::get('singular',['publicacion','uses' => 'PublicationsController@SinglePublication']);


Route::get('page', ['contacto', 'uses' => 'ContactController@contact']);

Route::get('page', ['copper-alliance-websites', 'uses' => 'WebsitesController@websites']);


Route::get('/signin', function()
{
    return view('signin');
});

Route::get('page', ['miembros-articulos', 'uses' => 'MembersArticlesController@membersArticles']);
Route::get('page', ['miembros-documentos', 'uses' => 'MembersDocumentsController@documents']);



Route::get('/miembro-videos', function()
{
    return view('miembro-videos');
});

Route::get('/resultado-de-busqueda', function()
{
    return view('resultado-de-busqueda');
});

Route::get('/page-404', function()
{
    return view('page-404');
});

Route::get('/page-404-users', function()
{
    return view('page-404-users');
});

Route::get('search', [ 'uses' => 'SearchController@result']);