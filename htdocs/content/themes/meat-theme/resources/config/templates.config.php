<?php

return [

    /**
     * Edit this file in order to configure your theme templates.
     *
     * Simply define a template slug.
     *
     * Since WordPress 4.7, templates can be defined for pages but
     * also on custom post types.
     */
    'custom-template' => ['Custom template', ['page']],
    'index' => ['Inicio', ['page']],
    'el-cobre-subpage' => ['Info Cobre', ['page']],
    'beneficios-sociales' => ['Beneficios Sociales', ['page']],
    'beneficios-subpage' => ['Beneficios Esencialidad', ['page']],
    'beneficios-suste' => ['Beneficios Sustentabilidad', ['page']],
    'beneficios-reci' => ['Beneficios Reciclabilidad', ['page']],
    'nosotros' => ['Nosotros', ['page']],
    'publicaciones' => ['Publicaciones', ['page']],
    'publicaciones-page' => ['Publicaciones Page', ['page']],
    'contacto' => ['contacto', ['page']],
    'copper-alliance-website' => ['Copper Websites', ['page']],
    'miembros-articulos' => ['Miembros Articulos', ['page']],
    'miembros-videos' => ['Miembros Videos', ['page']],
    'miembros-presentaciones' => ['Miembros Presentaciones', ['page']],
    'trens-innovations' => ['Trens Innovations', ['page']],

];
