<?php
Ajax::listen('filter_publications', function() {

    $args = array(
        'post_type'=>'publicacion',
        'showposts' => -1,
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'Categorias',
                'field'    => 'slug',
                'terms'    => $_POST['current_cat'],
            ),
            array(
                'taxonomy' => 'cat_type',
                'field'    => 'slug',
                'terms'    => $_POST['cat'],
            ),
        ),
    );

    $query1 = new WP_Query( $args );
    if ($query1->have_posts()) {
        $elemento='';
        $cont = 1;
        while ($query1->have_posts()) {
            $query1->the_post();

            $image_featured =  get_field('featured_image');
            if(!empty($image_featured)){
                $image_featured = $image_featured['url'];
            }else{
                $image_featured = '';
            }

            $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];
            $type_cat = $termsType->name;

            $short_description = get_field('short_description');

            echo '
            <div class="target-last-news set-target-documento-detalle ">

                <div class="target-last-news-image">
                    <img src="'.$image_featured.'" alt="">
                </div>
                <div class="button-float">
                    <p>'.$type_cat.'</p>
                </div>
                <div class="target-last-news-cuerpo">
                    <h3>'.get_the_title().'</h3>
                </div>
                <div class="target-last-news-time">
                    <i class="fa fa-clock-o" aria-hidden="true"><span>'.get_the_date().'</span></i>
                </div>
                <div class="target-last-news-text">'.$short_description.'</div>
                <a href="'.get_the_permalink().'" class="button super-button"><span>VER MÁS</span></a>
            </div>
            ';
        }
    }else{
        echo 'No se encontraron productos bajo ese término de búsqueda';
    }

    die();
});
