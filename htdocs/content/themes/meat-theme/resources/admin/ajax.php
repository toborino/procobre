<?php 

Ajax::listen('contact', function () {

	$response   = new stdClass();
	$response->success = true;
	$response->message = [
		'type'	 	=> 'success',
		'title' 	=> 'Envio Exitoso',
		'text' 		=> 'Gracias por contactarnos',
		'button'	=> 'aceptar'
	];

	echo json_encode($response);
	
	die();
});