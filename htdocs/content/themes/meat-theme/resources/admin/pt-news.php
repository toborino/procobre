<?php
PostType::make('publicacion', 'Publicaciones', 'Publicaciones')->set([
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'show_in_nav_menus'  => true,
    'query_var'          => true,
    'rewrite'         => array('slug' =>'publicacion'),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => true,
    'menu_icon'           => 'dashicons-media-text',
    'supports'           => ['title', 'thumbnail', 'revisions'],
]);

Taxonomy::make('Categorias', 'publicacion', 'Categoria', 'Categoria')->set([
    'public'       => true,
    'rewrite'      => false,
    'query_var'    => false,
    'hierarchical' => true,
    'rewrite' => array( 'with_front' => false ),
]);

Taxonomy::make('cat_type', 'publicacion', 'Tipo', 'Tipo')->set([
    'public'       => true,
    'rewrite'      => false,
    'query_var'    => false,
    'hierarchical' => true,
    'rewrite' => array( 'with_front' => false ),
]);