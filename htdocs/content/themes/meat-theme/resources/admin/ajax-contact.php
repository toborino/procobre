<?php
Ajax::listen('form_contact', function() {
    global $wpdb;
    $response   = new stdClass();
    $arr = [];
    $validator = validate(Input::all(), [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'messages' => 'required'
        ]
    );

    if($validator->fails()) {
        //$arr = $validator->errors();
        $response->success = false;
        $response->message = [
            'type' => 'Error',
            'title' => 'Error en el envio.',
            'text' => 'A ocurrio un problema al intentar enviar su correo, intente nuevamente',
            'type' => 'warning',
            'button' => 'Rechazado'
        ];
        echo json_encode($response);
        die();
    }

    $subjectMail = explode('|', $_POST['subject']);

    $mandrill 	= new Mandrill(getenv('MANDRILL_API_KEY'));
    $table 		= $wpdb->prefix . 'contact';

    # Format data for DB.
    $data_db = [
        'name' => $_POST['name'],
        'lastname' => $_POST['lastname'],
        'subject'  => $_POST['subject'],
        'email'   => $_POST['email'],
        'message'	=> $_POST['messages'],
        'date'	=> date("Y-m-d H:i:s")
    ];

    #Save in DB
    $wpdb->insert($table, $data_db);
    $message = [
        'from_email' 		  => 'no-reply@partners.cl',
        'from_name' 		  => 'Contacto',
        'subject' 			  => $_POST['subject'],
        'preserve_recipients' => FALSE,
    ];


    $message['to'] = [
        ['email' => $_POST['email_destino']]
    ];


    $vars_for_template = [
        'FORMULARIO'		=> 'Formulario de Contacto',
        'FIRSTNAME'		=> $_POST['name'],
        'LASTNAME'		=> $_POST['name'],
        'SUBJECT'	=> $_POST['subject'],
        'EMAIL'		=> $_POST['email'],
        'MENSAJE'	=> $_POST['messages']
    ];

    foreach($vars_for_template as $key => $value):
        $message["global_merge_vars"][] = [
            'name' 		=> $key,
            'content' 	=> $value
        ];
    endforeach;


    #Send Mail
    $mail = $mandrill->messages->sendTemplate('form-contact-copper', [] ,$message);

    if ($mail){
        $response->success = true;
        $response->message = [
            'type' => 'success',
            'title' => 'Envio Exitoso',
            'text' => $_POST['name']. ', gracias por contactarnos',
            'type' => 'info',
            'button' => 'Aceptar'
        ];
    } else {
        $response->success = false;
        $response->message = [
            'type' => 'Error',
            'title' => 'Error en el envio.',
            'text' => 'A ocurrio un problema al intear enviar su correo.',
            'type' => 'warning',
            'button' => 'Rechazado'
        ];
    }

    echo json_encode($response);

    die();
});