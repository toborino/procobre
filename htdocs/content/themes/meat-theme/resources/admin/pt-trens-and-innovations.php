<?php
PostType::make('trens-innovations', 'Trens and Innovations', 'Trens and Innovations')->set([
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'show_in_nav_menus'  => true,
    'query_var'          => true,
    //'rewrite'         => array('slug' =>'trens-and-innovations'),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'orderby' => 'menu_order',
    'menu_position'      => true,
    'menu_icon'           => 'dashicons-admin-generic',
    'supports'           => ['title', 'thumbnail', 'revisions'],
]);