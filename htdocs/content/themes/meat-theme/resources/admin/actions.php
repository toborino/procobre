<?php
// allow SVG in media
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function wp_nav_menu_top()
{
    $options = array(
        'echo' => false,
        'container' => false,
        'theme_location' => 'Menu Top',
        'menu' => 'Menu Top'
    );

    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);

}


acf_add_options_page(array(
    'page_title'    => 'Opciones Generales',
    'menu_title'    => 'Opciones Generales',
    'menu_slug'     => 'theme-general-settings',
    'capability'    => 'edit_posts',
    'redirect'      => true
));


acf_add_options_sub_page(array(
    'page_title'    => 'Editar Footer',
    'menu_title'    => 'Footer',
    'parent_slug'   => 'theme-general-settings',
));

acf_add_options_sub_page(array(
    'page_title'    => 'Editar Otros',
    'menu_title'    => 'Otros',
    'parent_slug'   => 'theme-general-settings',
));


function auto_login() {
    $username = $user_id;
    if ( !is_user_logged_in() ) {
        $user = get_userdatabylogin( $username );
        $user_id = $user->ID;
        wp_set_current_user( $user_id, $user_login );
        wp_set_auth_cookie( $user_id );
        do_action( 'wp_login', $user_login );
        wp_redirect( '/registro' );
    }
    die();
}
add_action('wp_ajax_auto_login', 'auto_login');
add_action('wp_ajax_nopriv_auto_login', 'auto_login');

//Add class imagen content post page default
/*
function changeImageMarkUp ($html, $id, $caption, $title, $align, $url) {
    $url = wp_get_attachment_url($id);
    $html = '<div class="video animate animate-in" style="background-image: url('.$url.');">';
    $html .= '<img style="display:none" src="'.$url.'" alt="'.$title.'">';
    $html .= '</div>';
    return $html;
}
add_filter( 'image_send_to_editor', 'changeImageMarkUp', 10, 9 );
*/