<?php
Ajax::listen('paginator_publication', function() {
    global $wpdb;

    $page  = $_POST['page'];
    $limit = $_POST['limit'];

    if ($page > 1) {
        $offset = $page - 1;
        $offset = $offset * $limit;

    }else{
        $offset = 0;
    }

    $args = array(
        'post_type'=>'publicacion',
        'tax_query' => array(
            array(
                'taxonomy' => 'Categorias',
                'field'    => 'slug',
                'terms'    => 'energias-renovables',
            ),
        ),
        'posts_per_page' => $limit,
        'offset'		 => $offset
    );

    $query1 = new WP_Query( $args );
    if ($query1->have_posts()) {
        $elemento='';
        $cont = 1;
        while ($query1->have_posts()) {
            $query1->the_post();

            $image_featured =  get_field('featured_image');
            if(!empty($image_featured)){
                $image_featured = $image_featured['url'];
            }else{
                $image_featured = '';
            }

            $termsType = get_the_terms( get_the_ID(), 'cat_type' )[0];
            $type_cat = $termsType->name;

            $short_description = get_field('short_description');

            echo '
            <div class="target-last-news set-target-documento-detalle animate">

                <div class="target-last-news-image">
                    <img src="'.$image_featured.'" alt="">
                </div>
                <div class="button-float">
                    <p>'.$type_cat.'</p>
                </div>
                <div class="target-last-news-cuerpo">
                    <h3>'.get_the_title().'</h3>
                </div>
                <div class="target-last-news-time">
                    <i class="fa fa-clock-o" aria-hidden="true"><span>'.get_the_date().'</span></i>
                </div>
                <div class="target-last-news-text">'.$short_description.'</div>
                <a href="'.get_the_permalink().'" class="button super-button"><span>VER MÁS</span></a>
            </div>
            ';
        }
    }else{
        echo 'No se encontraron productos bajo ese término de búsqueda';
    }


    $count_post = array(
        'post_type' => 'publicacion',
        'showposts' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'Categorias',
                'field'    => 'slug',
                'terms'    => 'energias-renovables',
            ),
        ),
    );

    $cantidad = get_posts($count_post);

    $pp=1;
    $total = count($cantidad);
    $current_page =1;
    $page_amount = $total/2;
    $limiter = 8;

    $sides = round(($limiter/2), 0, PHP_ROUND_HALF_DOWN);

    $anterior = $page - 1;
    $siguiente = $page + 1;

    ?>
    <div class="content-paginator ">
        <div class="nav-links pagitantionProduct">
            <a class="prev page-numbers" data-pp="<?php echo $pp ?>" data-accion="anterior" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
            <div style="display: none;" class="page_actual">1</div>
            <div style="display: none;" class="page_total"><?php echo $page_amount ?></div>

            <?php
            if($page == 2){
                echo  '<a class=" page-numbers set-border" href="javascript:void(0)" id="pv1" data-page="1">1 </a> ';
            }
            if ($current_page > ($limiter/2)){
                echo  '<a class=" page-numbers set-border" href="javascript:void(0)" id="pv1" data-page="1">1 </a><a> ...</a> ';
            }
            for ( $counter = 1; $counter <=  $page_amount; $counter++) {

                if($counter >= ($current_page)){
                    if(($counter <($current_page+$sides))||($counter >($page_amount-$sides))){
                        $num_page = $counter+1;
                        if($page == $num_page){
                            $current = 'current';
                        }else{
                            $current = '';
                        }
                        echo '<a class=" page-numbers set-border '.$current.'" id="pv'.$num_page.'" href="javascript:void(0)" data-pp="'.$pp.'" data-page="'.$num_page.'">'.$num_page.'</a>';
                    }elseif($counter ==($current_page+$sides)){
                        $num_page = $counter+1;
                        if($page == $num_page){
                            $current = 'current';
                        }else{
                            $current = '';
                        }
                        echo '<a class=" page-numbers set-border '.$current.' " id="pv'.$num_page.'" href="javascript:void(0)" data-pp="'.$pp.'" data-page="'.$num_page.'">';

                        if(($page_amount-$current_page)==$limiter-1){
                            echo $counter+1;
                        }else {
                            echo "...";
                        }
                        echo "</a> ";
                    }
                }
            }
            ?>

            <a class="next page-numbers" data-pp="<?php echo $pp ?>" data-accion="siguiente" href="#"> <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
<?php

    die();
});