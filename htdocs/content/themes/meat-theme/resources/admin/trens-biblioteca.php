<?php
PostType::make('trens-biblioteca', 'Trens Biblioteca', 'Trens Biblioteca')->set([
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'show_in_nav_menus'  => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'orderby' => 'menu_order',
    'menu_position'      => true,
    'menu_icon'           => 'dashicons-admin-generic',
    'supports'           => ['title', 'thumbnail', 'revisions'],
]);

Taxonomy::make('trens_cat', 'trens-biblioteca', 'Categoria', 'Categoria')->set([
    'public'       => true,
    'rewrite'      => false,
    'query_var'    => false,
    'hierarchical' => true,
    'rewrite' => array( 'with_front' => false ),
]);