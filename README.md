# Procobre - sitio LA
by [Digital Meat](http://meat.cl/)
Mantido por Toborino

### Manual installation
#### Development / Local Environment
```bash
git clone git@bitbucket.org:toborino/project.git
cd project
npm install
composer install --prefer-dist
cp .env.example .env
vim .env  
npm run watch
php artisan serve
```

#### Production Environments
```bash
git clone git@bitbucket.org:toborino/project.git
cd project
npm install
composer install --prefer-dist --no-dev
npm run production
cp .env.example .env
vim .env  
```
#### Deploy script
```bash
cd /home/forge/procobre.com
git pull origin develop
composer install --no-interaction --prefer-dist --optimize-autoloader
npm i
npm run production
```